<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pagrindinis_puslapis');
});

Route::get('/pagrindinis_puslapis', function () {
    return view('pagrindinis_puslapis');
});

Route::get('/fakt__baz_', function () {
    return view('fakt__baz_');
});

Route::get('/registracija', function () {
    return view('registracija');
});

Route::get('/nustatymai', function () {
    return view('nustatymai');
});

Route::get('/mokamos_paslaugos', function () {
    return view('mokamos_paslaugos');
});

Route::get('/nemokamos_paslaugos', function () {
    return view('nemokamos_paslaugos');
});

Route::get('/aukojimas', function () {
    return view('aukojimas');
});

Route::get('/taisykl_s', function () {
    return view('taisykl_s');
});

Route::get('/kontaktai', function () {
    return view('kontaktai');
});

Route::get('/prid_ti_kategorij_', function () {
    return view('kategorijos.prid_ti_kategorij_');
});

Route::get('/vartotojo_s_saja', 'UserController@index'
/*
    function () {
    return view('vartotojo_s_saja');
}
*/
);

Route::post('/pagrindinis_puslapis/reg', 'UserController@create');

Route::get('/prisijungimas', 'UserController@logview');

Route::get('/prisijungimas/duom', 'UserController@logtest');

Route::get('/prisijungimas/baigti', 'UserController@logend');

Route::get('/blogo__ra_o_ra_ymas', 'BlogController@create');

Route::post('/blogo__ra_ai/create', 'BlogController@store');

Route::post('/paie_ka', 'SearchController@contentsearch');

Route::get('/blogo__ra_ai', 'BlogController@index');

Route::post('/kategorijos', 'CategoryController@store');

Route::get('/kategorijos', 'CategoryController@index')->name('categoryindex');

Route::get('/kategorijos/{category}', 'CategoryController@show')->name('showcategory');

Route::get('/prid_ti_subkategorij_', 'SubcategoryController@create');

Route::post('/subkategorijos', 'SubcategoryController@store');

Route::get('/turinys', 'ArchiveController@index');

Route::get('/paie_ka', 'SearchController@index');

Route::post('/papildoma_subkategorija', 'SubcategoryController@extrasubcategories');

Route::post('/papildoma_kategorija', 'CategoryController@extracategories');




