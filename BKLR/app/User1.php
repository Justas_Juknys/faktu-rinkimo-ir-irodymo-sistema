<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User1 extends Model
{
    protected $fillable =[
        'user_nickname', 'user_birth_date','user_password','user_email','user_email','user_name',
        'user_surname','user_address', 'user_telephone','user_payment_method','user_sex',
        'user_political_alignment','user_spiritual_beliefs','user_profession','user_email',
        'user_extra_information','user_hyperlink'
    ];
    // protected $dispatchesevents=[
    //      'created' => ProjectCreated::class
    //  ];
    protected $primaryKey = 'user_id';
    //public $table = "step_list";



    protected static function boot(){
        parent::boot();

        static::created(function ($step){
            #return 0;
            #excecutes every single time a project is created
        }

        );
    }

    /*

        public function owner(){
            return $this->belongsTo(User::class);
        }
    */
    /*
        public function tasks(){
            return $this->hasMany(Task::class);
        }
        // protected $guarded others are fillable except guarded ones

        public function addTask(//$task
            $description
        )
        {

            //$this->tasks()->create($task);
            //$this->tasks()->create(compact('description'));
            //dd($description);

            return Task::create([
                'project_id' => $this->id,
                'body' => $description
            ]);
        }
    */
}

