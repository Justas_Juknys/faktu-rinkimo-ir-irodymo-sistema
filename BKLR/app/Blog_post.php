<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class blog_post extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'blog_text', 'blog_author_id', 'blog_topic', 'blog_illustration', 'blog_category_id', 'blog_word_list', 'blog_word_frequency', 'blog_word_total', 'blog_bigram_list', 'blog_trigram_list','blog_word_count', 'blog_category_list', 'blog_subcategory_list'
    ];

    protected $primaryKey = 'blog_id';

    public function authors(){
        return $this->belongsTo('App\user1','blog_author_id');
    }

    public function categories(){
        return $this->belongsTo('App\category_list','blog_category_id');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
