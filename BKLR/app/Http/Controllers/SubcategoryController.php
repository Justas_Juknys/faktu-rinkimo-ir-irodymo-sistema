<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User1;
use App\Blog_post;
use App\Subcategory_list;
use App\category_list;
use Illuminate\Support\Arr;

class SubcategoryController extends Controller
{
    public function create(){
        $req=request()->category_id;
        //dd($req);
return view('subkategorijos.prid_ti_subkategorij_',compact('req'));
    }
    public function store(){
        //dd(request());
        $req=request()->validate([
            'subcategory_title' => 'required',
            'subcategory_description' => '',
            'category_id' => 'required',
        ]);
        //dd($req);
        //$address= '/BKLR/public/kategorijos/'.$req['category_id'];
        //dd($address);
        Subcategory_list::create($req);
        return redirect()->route('showcategory',$req['category_id']);


    }

    public function index(){
        $categories=Subcategory_list::all();
        return view('kategorijos.kategorijos', compact('categories'));
    }

    public function show($category){
        //dd($category);
        //dd(Category_list::all()->where('category_id', $category));
        $rez=Subcategory_list::all()->where('category_id', $category)->first();
        return view('kategorijos.rodyti_kategorij_',compact('rez'));
    }

    public function extrasubcategories(){
        //dd(request());
        //dd(Blog_post::all()->where('blog_id',request()->blog_id))->first()->blog_category_list;
        $blog_id=request()->blog_id;
        $blog_categories=unserialize(Blog_post::all()->where('blog_id',$blog_id)->first()->blog_category_list);
        array_push($blog_categories,(int)request()->category_id);
        //dd($blog_categories);
        Blog_post::all()->where('blog_id',$blog_id)->first()->update(['blog_category_list' => serialize($blog_categories)]);
        //dd('done');
        //dd(unserialize(Blog_post::all()->where('blog_id',request()->blog_id)->first()->blog_category_list));
        $subcategories=Category_list::all()->where('category_id',request()->category_id)->first()->subcategories;
        $subcategory_max_id = $subcategories->max('subcategory_id');
        return view('subkategorijos.papildoma_subkategorija',compact('blog_id','subcategories','subcategory_max_id'));
        //dd($subcategories);
    }

}
