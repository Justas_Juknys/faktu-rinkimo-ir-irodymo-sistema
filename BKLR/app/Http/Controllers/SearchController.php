<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User1;
use App\Blog_post;
use App\Category_list;

class SearchController extends Controller
{
    public function index(){
        //dd('hello');
        $categories=Category_list::all();
        return view('paie_ka',compact('categories'));
    }

public function contentsearch()
{
//dd(request()->search_text);
    $words = [];
    $bwords = [];
    $twords = [];
    $count = [];
    $freq = [];
    $rezcos = [];
    $search_word_total = 0;
    $blog_id=[];
    $blog_word_list=[];
    $blog_word_frequency=[];
    $blog_bigram_list=[];
    $blog_trigram_list=[];
    $result=collect();



    $txt = request()->search_text;
    //$rez = preg_split("/[\s,.]+/",$txt);
    //dd($rez);
    $txtrez=preg_split("/[\s,.]+/",$txt);
    //dd($txtrez);
    $size=sizeof($txtrez);
    //dd($txtrez[0],$txtrez[1],$txtrez[2]);
    if($size>0)
    {
        array_push($words, $txtrez[0]);
        array_push($count, 1);
        $search_word_total++;
        if ($size>1)
        {
            array_push($bwords, $txtrez[0] . ' ' . $txtrez[1]);
            if ($txtrez[0] != $txtrez[1])
            {
                array_push($words, $txtrez[1]);
                array_push($count, 1);
                $search_word_total++;
            }
            else
            {
                $count[0]++;
            }
            if ($size > 2)
            {
                for ($i = 2; $i < $size; $i++)
                {
                    $tword = $txtrez[$i - 2] . ' ' . $txtrez[$i - 1] . ' ' . $txtrez[$i];
                    //dd($size);
                    $bword = $txtrez[$i - 1] . ' ' . $txtrez[$i];
                    //dd($twords, $tword);
                    if (array_search($tword, $twords) == false)
                    {
                        array_push($twords, $tword);
                    }
                    if (array_search($bword, $bwords) == false)
                    {
                        array_push($bwords, $bword);
                    }

                    $rez = array_search($txtrez[$i], $words);
 //                   array_push($rezc, $rez);
 //                   if(0===false)dd('test');

                    if ($rez!==false)
                    {
                        //dd('test');
                        $count[$rez] = $count[$rez] + 1;
                    }
                    else
                    {
                        array_push($words, $txtrez[$i]);
                        array_push($count, 1);
                    }
                    $search_word_total = $search_word_total + 1;


                }
            }
        }
    }

    for ($i = 0; $i<sizeof($count); $i++)
    {
        $freq[$i]=$count[$i]/$search_word_total;
    }

    $blogposts=Blog_post::all();
    foreach($blogposts as $blogpost)
    {
        $search_square=0;
        $blog_square=0;
        $multiplication=0;
        //$cos=0;
        $blog_words=unserialize($blogpost->blog_word_list);
        $blog_count=unserialize($blogpost->blog_word_count);
        for($i=0;$i<sizeof($words); $i++)
        {
            $search_square=$search_square+$count[$i]*$count[$i];

            $found_place=array_search($words[$i], $blog_words);
            if($found_place!==false)$found_value=$blog_count[$found_place];
            else $found_value=0;
            $multiplication=$multiplication+$count[$i]*$found_value;
        }
        for($i=0;$i<sizeof($blog_words); $i++)
        {
            $blog_square=$blog_square+$blog_count[$i]*$blog_count[$i];
        }

        $cos=$multiplication/sqrt($search_square)/sqrt($blog_square);
        array_push($rezcos, $cos);
        if ($cos>0)
        {

            //$blogpost=$blogpost::all()->where('blog_id',$blogpost->blog_id)->put('cos',$cos);
            //$blogpost=$blogpost->where('blog_id',$blogpost->blog_id);
            //$blogpost=$blogpost::all()->where('blog_id',$blogpost->blog_id)->put(['cos' => $cos]);
            $blogpost->setAttribute('cos',$cos);
            //$result->push($blogpost);

        }

        /*
        array_push($blog_id, $blogpost->blog_id);
        array_push($blog_word_list, $blogpost->blog_word_list);
        array_push($blog_word_frequency, $blogpost->blog_word_frequency);
        array_push($blog_bigram_list, $blogpost->blog_bigram_list);
        array_push($blog_trigram_list, $blogpost->blog_trigram_list);
        */
    }
    $blogposts=$blogposts->sortByDesc('cos');
    //$result=$result['cos']->sortBy('cos','ASC');
    //return view('paie_ka',compact('blogposts'));

    //redirect ('/paie_ka');

    $categories=Category_list::all();

    return view('paie_ka',compact('blogposts'),compact('categories'));
    //dd($rezcos);
    //dd($blog_id,$blog_word_list,$blog_word_frequency,$blog_bigram_list,$blog_trigram_list);

//dd($rezc);
    //dd($words,$count,$freq,$bwords,$twords);

//dd($twords);
/*
    foreach(preg_split("/[\s,.]+/",$txt) as $word){
        //$words = Arr::add($word);
        //if(array_search('bacon', $words)) dd('bacon found');
        $rez=array_search($word,$words);
        if ($rez){
            $count[$rez]=$count[$rez]+1;
        }
        else {
            array_push($words, $word);
            array_push($count, 1);
        }
        $blog_word_total=$blog_word_total+1;
    }
*/

}
}
