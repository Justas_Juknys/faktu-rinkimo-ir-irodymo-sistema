<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User1;

class UserController extends Controller
{
    public function create(){

        request()->validate([
            'user_nickname' => 'required',
            'user_birth_date' => 'required',
            'user_password' => 'required',
            'user_email' => 'required',
            'user_password' => 'required_with:user_password_repeated|same:user_password_repeated',
            'user_password_repeated' => 'required'
        ]);

        $req=request()->except(['user_password_repeated','user_confirmation_code']);

        User1::create($req);
        session(['user'=>request('user_nickname'), 'user_registered'=>'1', 'userid' =>User1::all()->last()->user_id]);

        return redirect('pagrindinis_puslapis');

    }

    public function index(){
        $user=User1::all()->where('user_id',session('userid'))->first();
        //dd ($user);
        //dd(session('userid'));

        return view('vartotojo_s_saja', [
            'user' => $user]);
    }

    public function logview(){
        return view('prisijungimas');
    }

    public function logtest(){
        //dd(request('user_password'));
        $user=User1::all()->firstwhere('user_nickname',request('user_nickname'));
        //dd(request());
        //dd($user->user_password);
        //dd($user);
        if(request('user_password')==$user->user_password)
        {
            //dd('TAIP');
            session(['user'=>request('user_nickname'), 'user_registered'=>'1', 'userid' =>$user->user_id]);
            //dd(session());
            //dd(session('user','NERA'));
            return redirect('pagrindinis_puslapis');

        }
        else{
            //dd('NE');
        }


    }

    public function logend(){
        //dd('labas');
        session()->flush();
        return redirect('pagrindinis_puslapis');
    }
}
