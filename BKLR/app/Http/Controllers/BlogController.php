<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User1;
use App\Blog_post;
use Illuminate\Support\Arr;
use App\Category_list;
use App\Subcategory_list;

class BlogController extends Controller
{
    public function create(){
        //dd('hello');
        $categories=Category_list::all();
        return view('blogo__ra_o_ra_ymas', compact('categories'));
    }

    public function store()
    {
        //dd('hello');
        //dd(request());
        //dd(session());
        //dd(request('blog_text'), request('blog_author_id'), request('blog_topic'), request('blog_category_id'), request('blog_illustration'));

        request()->validate([
            'blog_text' => 'required',
            'blog_author_id' => 'required',
            'blog_topic' => 'required',
            'blog_category_id' => 'required',
            'blog_illustration' => 'required'
        ]);

        $req = request()->except(['sriftas', 'sypseneles']);
        //dd($req);
        //$delim = [' ', '.', ',', '?', '!', ':', ';', '/'];
        $words = [];
        $bwords = [];
        $twords = [];
        $count = [];
        $freq = [];
        $blog_word_total = 0;

        $txt = $req['blog_text'];
        //$rez = preg_split("/[\s,.]+/",$txt);
        //dd($rez);
        $txtrez = preg_split("/[\s,.]+/", $txt);
        $size = sizeof($txtrez);

        if ($size > 0)
        {
            array_push($words, $txtrez[0]);
            array_push($count, 1);
            $blog_word_total++;
            if ($size > 1)
            {
                array_push($bwords, $txtrez[0] . ' ' . $txtrez[1]);
                if ($txtrez[0] != $txtrez[1])
                {
                    array_push($words, $txtrez[1]);
                    array_push($count, 1);
                    $blog_word_total++;
                }
                else
                    {
                    $count[0]++;
                    }
                if ($size > 2) {
                    for ($i = 2; $i < $size; $i++)
                    {
                        $tword = $txtrez[$i - 2] . ' ' . $txtrez[$i - 1] . ' ' . $txtrez[$i];
                        $bword = $txtrez[$i - 1] . ' ' . $txtrez[$i];
                        if (array_search($tword, $twords) == false)
                        {
                            array_push($twords, $tword);
                        }
                        if (array_search($bword, $bwords) == false)
                        {
                            array_push($bwords, $bword);
                        }

                        $rez = array_search($txtrez[$i], $words);
                        if ($rez !== false) {
                            $count[$rez] = $count[$rez] + 1;
                        }
                        else
                            {
                            array_push($words, $txtrez[$i]);
                            array_push($count, 1);
                            }
                        $blog_word_total = $blog_word_total + 1;


                    }
                }
            }
        }


        for ($i = 0; $i < sizeof($count); $i++)
        {
            $freq[$i] = $count[$i] / $blog_word_total;
        }
        $blog_word_list = serialize($words);
        $blog_word_frequency = serialize($freq);
        $blog_bigram_list = serialize($bwords);
        $blog_trigram_list = serialize($twords);
        $blog_word_count = serialize($count);

        //Blog_post::create($req);
        //$rez=(array_merge($req, ['blog_word_list' => $blog_word_list, 'blog_word_frequency' => $blog_word_frequency, 'blog_word_total' => $blog_word_total]));
        //dd($rez);
        //dd($count);
        #Blog_post::create((array_merge($req, ['blog_word_list' => $blog_word_list, 'blog_word_frequency' => $blog_word_frequency, 'blog_word_total' => $blog_word_total, 'blog_bigram_list' => $blog_bigram_list, 'blog_trigram_list' => $blog_trigram_list,'blog_word_count' => $blog_word_count])));
//dd(request());
        // dd(request()->has('progress'));
        $category_list = [];
        //$subcategory_list = [];
        //$subcategory_lists = [];
        $categories = Category_list::all();


        if (request()->has('progress'))
        {
            $foundid = $categories->where('category_id', $req['blog_category_id'])->first()->category_id;
            array_push($category_list, $foundid);
        }
        $prepared_categories = serialize($category_list);
        //$prepared_subcategory = serialize($subcategory_list);
        //array_push($subcategory_lists, $prepared_subcategory);
        //$prepared_subcategories=serialize($subcategory_lists);
        //dd($prepared_subcategories, unserialize($prepared_subcategories));

        Blog_post::create((array_merge($req, ['blog_word_list' => $blog_word_list, 'blog_word_frequency' => $blog_word_frequency, 'blog_word_total' => $blog_word_total, 'blog_bigram_list' => $blog_bigram_list, 'blog_trigram_list' => $blog_trigram_list, 'blog_word_count' => $blog_word_count, 'blog_category_list' => $prepared_categories])));
        $blog_id=Blog_post::all()->last()->blog_id;
        $subcategories=Category_list::all()->where('category_id',request()->blog_category_id)->first()->subcategories;
        $subcategory_max_id = $subcategories->max('subcategory_id');
        //dd($blog);
        if (request()->has('progress'))
        {
            return view('subkategorijos.papildoma_subkategorija', compact('blog_id','subcategories','subcategory_max_id'));
        } else
            {
            return redirect('pagrindinis_puslapis');
        }



    }

    public function addcategory (){
        dd('hello');
    }

    public function index()
    {
        //dd(Blog_post::all());
        $newest=Blog_post::all()->last();


        $allcategories=Category_list::all();
        $allsubcategories=Subcategory_list::all();
        $category_names=[];

        $categories=unserialize($newest->blog_category_list);
        foreach($categories as $category)
        {

            array_push($category_names,$allcategories->where('category_id',$category)->first()->category_title);
        }
       // dd($category_names);

        $subcategory_names = [];
        $subcategory_name_sets=[];
        $subcategory_sets=unserialize($newest->blog_subcategory_list);
        foreach($subcategory_sets as $subcategory_set)
        {
            foreach(unserialize($subcategory_set) as $subcategory)
            {
                array_push($subcategory_names,$allsubcategories->where('subcategory_id',$subcategory)->first()->subcategory_title);
            }
            array_push($subcategory_name_sets,serialize($subcategory_names));
            $subcategory_names=[];
        }
        //dd($subcategory_name_sets);
        //dd(sizeof($subcategory_name_sets));
        $used_category_amount=sizeof($subcategory_name_sets);
        return view ('blogo__ra_ai', compact('newest','category_names', 'subcategory_name_sets','categories','subcategory_sets','used_category_amount'));
    }
}
