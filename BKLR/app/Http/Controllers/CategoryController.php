<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User1;
use App\Blog_post;
use App\Category_list;
use Illuminate\Support\Arr;

class CategoryController extends Controller
{
    public function store()
    {
        //dd(request());
        $req=request()->validate([
            'category_title' => 'required',
            'category_description' => ''
        ]);
        //dd($req);
        //dd('hi');
        Category_list::create($req);
        return redirect()->route('categoryindex');
    }

    public function index()
    {
        $categories=Category_list::all();
        return view('kategorijos.kategorijos', compact('categories'));
    }

    public function show($category)
    {
        //dd($category);
        //dd(Category_list::all()->where('category_id', $category));
        $rez=Category_list::all()->where('category_id', $category)->first();
        return view('kategorijos.rodyti_kategorij_',compact('rez'));
    }

    public function extracategories(){
        //dd(request());
        $req=request();
        $blog_id=$req->blog_id;
        $selected_subcategories = [];
        for($i=1;$i<=$req->subcategory_max_id;$i++)
        {
            if($req->has('sub'.$i))
            {
                array_push($selected_subcategories,$i);
            }
        }
        //dd($selected_subcategories);

        //dd(unserialize(Blog_post::all()->where('blog_id',$req->blog_id)->first()->blog_subcategory_list));
        $subcategory_sets=unserialize(Blog_post::all()->where('blog_id',$blog_id)->first()->blog_subcategory_list);
        //dd($subcategory_sets);
        if($subcategory_sets===false)
        {
            $subcategory_sets[0]=serialize($selected_subcategories);
        }
        else
        {
            array_push($subcategory_sets, serialize($selected_subcategories));
        }
        //dd($subcategory_sets);
        Blog_post::all()->where('blog_id',$blog_id)->first()->update(['blog_subcategory_list' => serialize($subcategory_sets)]);
        //dd(Blog_post::all()->where('blog_id',$blog_id)->first()->blog_subcategory_list);

        //dd(unserialize(Blog_post::all()->where('blog_id',$blog_id)->first()->blog_category_list));
        $category_list=unserialize(Blog_post::all()->where('blog_id',$blog_id)->first()->blog_category_list);
        $categories=Category_list::all();
        return view('kategorijos.papildoma_kategorija', compact('category_list', 'categories','blog_id'));


    }

}
