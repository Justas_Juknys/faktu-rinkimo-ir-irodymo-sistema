<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TheoryListsDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theory_lists', function (Blueprint $table) {
            $table->increments('theory_id');
            $table->integer('subcategory_id')->unsigned();;
            $table->string('theory_name');
            $table->text('theory_content');
            $table->string('theory_source');
            $table->string('theory_author');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();

            $table->foreign('subcategory_id')->references('subcategory_id')->on('subcategory_lists');





        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_resets');
    }
}
