<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user1s', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('user_nickname');
            $table->date('user_birth_date');
            $table->string('user_password');
            $table->string('user_email');


            $table->string('user_name')->nullable();
            $table->string('user_surname')->nullable();
            $table->string('user_address')->nullable();
            $table->string('user_telephone')->nullable();
            $table->string('user_payment_method')->nullable();

            $table->tinyInteger('user_sex')->nullable();
            $table->string('user_political_alignment')->nullable();
            $table->string('user_spiritual_beliefs')->nullable();
            $table->string('user_profession')->nullable();
            $table->text('user_extra_information')->nullable();
            $table->string('user_hyperlink')->nullable();

            $table->boolean('user_deleted')->default('0');
            $table->string('token')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->tinyInteger('user_authorization')->nullable();





        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_resets');
    }
}
