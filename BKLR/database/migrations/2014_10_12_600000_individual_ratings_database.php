<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IndividualRatingsDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('individual_ratings', function (Blueprint $table) {
            $table->increments('individual_rating_id');
            $table->Integer('rating_id')->unsigned();
            $table->tinyInteger('rating_type');
            $table->tinyInteger('user_id');
            $table->Integer('rating_value');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();

            $table->foreign('rating_id')->references('rating_id')->on('ratings');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_resets');
    }
}
