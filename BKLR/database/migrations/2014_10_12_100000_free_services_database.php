<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FreeServicesDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('free_services', function (Blueprint $table) {
            $table->increments('free_service_id');
            $table->Integer('free_service_type');
            $table->Integer('free_service_option_amount')->default('1');
            $table->string('free_service_name');
            $table->Integer('free_service_available_amount')->nullable();
            $table->Integer('free_service_ordered_amount')->nullable();
            $table->date('free_service_available_from')->nullable();
            $table->date('free_service_available_to')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();





        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_resets');
    }
}
