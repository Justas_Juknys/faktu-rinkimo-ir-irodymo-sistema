<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConversationsDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversations', function (Blueprint $table) {
            $table->increments('conversation_id');
            $table->Integer('conversation_sender_id')->unsigned();
            $table->Integer('conversation_receiver_id')->unsigned();
            $table->string('conversation_topic');
            $table->Integer('conversation_type');
            $table->string('conversation_message_amount');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();

            $table->foreign('conversation_sender_id')->references('user_id')->on('user1s');
            $table->foreign('conversation_receiver_id')->references('user_id')->on('user1s');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_resets');
    }
}
