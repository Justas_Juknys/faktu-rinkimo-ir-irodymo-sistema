<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RatingsDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('rating_id');
            $table->tinyInteger('rating_type');
            $table->Integer('rating_destination_id');
            $table->Integer('rating_count')->default(0);
            $table->Integer('rating_sum')->default(0);
            $table->float('rating_average')->nullable();
            $table->Integer('rating_5_count')->default(0);
            $table->Integer('rating_4_count')->default(0);
            $table->Integer('rating_3_count')->default(0);
            $table->Integer('rating_2_count')->default(0);
            $table->Integer('rating_1_count')->default(0);
            $table->Integer('rating_0_count')->default(0);
            $table->boolean('rating_deleted')->default('0');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();





        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_resets');
    }
}
