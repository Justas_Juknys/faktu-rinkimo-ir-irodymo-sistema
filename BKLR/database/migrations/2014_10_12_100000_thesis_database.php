<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ThesisDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thesis', function (Blueprint $table) {
            $table->increments('thesis_id');
            $table->Integer('theory_id')->unsigned();
            $table->string('thesis_name');
            $table->text('thesis_content');
            $table->string('thesis_source');
            $table->string('thesis_author');
            $table->boolean('thesis_counter_argument');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();

            $table->foreign('theory_id')->references('theory_id')->on('theory_lists');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_resets');
    }
}
