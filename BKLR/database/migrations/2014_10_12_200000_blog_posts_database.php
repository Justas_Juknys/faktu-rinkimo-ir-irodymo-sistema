<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BlogPostsDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   //Schema::enableForeignKeyConstraints();
        Schema::create('blog_posts', function (Blueprint $table) {
            $table->increments('blog_id');
            $table->Integer('blog_author_id')->unsigned();
            $table->String('blog_topic');
            $table->Integer('blog_category_id')->unsigned();;
            $table->boolean('blog_relevant')->default('1');
            $table->string('blog_illustration');
            $table->text('blog_text');
            $table->float('blog_rating_id')->nullable();
            $table->boolean('blog_deleted')->default('0');
            $table->boolean('blog_last')->nullable();
            $table->string('blog_sources')->nullable();
            $table->Integer('blog_theory_id')->nullable();
            $table->Integer('blog_thesis_id')->nullable();
            $table->Integer('blog_fact_id')->nullable();
            $table->text('blog_word_list')->nullable();
            $table->text('blog_word_count')->nullable();
            $table->text('blog_word_frequency')->nullable();
            $table->Integer('blog_word_total')->nullable();
            $table->text('blog_bigram_list')->nullable();
            $table->text('blog_trigram_list')->nullable();
            $table->text('blog_category_list')->nullable();
            $table->text('blog_subcategory_list')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();

            /*
            Schema::table('user1s', function($table) {
                $table->foreign('user_id')->references('blog_author_id')->on('user1s');
            });*/


            $table->foreign('blog_author_id')->references('user_id')->on('user1s');
            $table->foreign('blog_category_id')->references('category_id')->on('category_lists');





        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_resets');
    }
}
