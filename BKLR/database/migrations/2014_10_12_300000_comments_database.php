<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CommentsDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('comment_id');
            $table->Integer('comment_type');
            $table->Integer('comment_destination_id');
            $table->integer('comment_user_id')->unsigned();
            $table->string('comment_title');
            $table->string('comment_text');
            $table->float('comment_rating_id')->nullable();
            $table->boolean('comment_deleted')->default('0');
            $table->boolean('comment_last')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();

            $table->foreign('comment_user_id')->references('user_id')->on('user1s');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_resets');
    }
}
