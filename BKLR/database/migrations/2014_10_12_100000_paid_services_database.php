<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaidServicesDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paid_services', function (Blueprint $table) {
            $table->increments('paid_service_id');
            $table->Integer('paid_service_type');
            $table->Integer('paid_service_option_amount')->default('1');
            $table->float('paid_service_cost');
            $table->string('paid_service_name');
            $table->Integer('paid_service_available_amount')->nullable();
            $table->Integer('paid_service_ordered_amount')->nullable();
            $table->Integer('paid_service_sold_amount')->nullable();
            $table->date('paid_service_available_from')->nullable();
            $table->date('paid_service_available_to')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();





        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_resets');
    }
}
