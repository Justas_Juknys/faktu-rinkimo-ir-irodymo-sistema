@extends('layout')
@section('title')
    Mokamos Paslaugos
    @endsection
@section('javasript')
    <script type="text/javascript">
        function rate()
        {
            var a = $('input[name=optradio]:checked').val();
            var fieldNameElement = document.getElementById('Rplace').innerText;
            document.getElementById('Rplace').innerHTML = a;
        }

        function Rerase()
        {
            $('input[name=optradio]').prop('checked',false);
            document.getElementById('Rplace').innerHTML = "Įvertinimas";
        }

        function addbold() {
            $(comment).surroundSelectedText("<b>", "</b>");
        }

        function additalic() {
            $(comment).surroundSelectedText("<i>", "</i>");
        }

        function addunderline() {
            $(comment).surroundSelectedText("<ins>", "</ins>");
        }

        function addlower() {
            $(comment).surroundSelectedText("<sub>", "</sub>");
        }

        function addupper() {
            $(comment).surroundSelectedText("<sup>", "</sup>");
        }

        function addhyperlink() {
            $(comment).surroundSelectedText("<a href=\"www.jusuadresas.com\">", "</a>");
        }

        function Cerase()
        {
            document.getElementById('comment').value = "";
        }

        function addemoticon(E)
        {
            var e = document.getElementById(E);
            $(comment).surroundSelectedText("", e.options[e.selectedIndex].text);
        }

        function addcolor(E)
        {
            var e = document.getElementById(E).value;
            var color = "<font color=" + e + ">";
            $(comment).surroundSelectedText(color,"</font>");
        }

        function addfont(E)
        {
            var e = document.getElementById(E).value;
            if (e==1) $(comment).surroundSelectedText("<nobr class=a>","</nobr>");
            else if (e==2) $(comment).surroundSelectedText("<nobr class=c>","</nobr>");
        }
    </script>
    @endsection
@section('main')
    <div class="container-fluid">
        <h1>Siūlomos Paslaugos</h1>
    </div>


    <div class="table-responsive">
        <table class=table>
            <thead class="thead-light">
            <tr>
                <th>Paslaugos Kategorija</th>
                <th>Paslaugos Pavadinimas</th>
                <th>Paslaugos Pasirinkimai</th>
                <th>Kaina</th>
                <th>Suteikimo Data</th>
                <th>Paslaugos Tiekėjas</th>
                <th>Ar Pasirinkimas Įmanomas?</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr style="background-color:orange;">
                <td>Reklamos Paslaugos</td>
                <td>Paslauga1</td>
                <td>  <select class="form-control" id="Option011" name="paslaugos_pasirinkimas1">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina1</td>
                <td>  <select class="form-control" id="Option012" name="datos_pasirinkimas1">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option013" name="tiekėjo_pasirinkimas1">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras1</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:orange;">
                <td></td>
                <td>Paslauga2</td>
                <td>  <select class="form-control" id="Option021" name="paslaugos_pasirinkimas2">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina2</td>
                <td>  <select class="form-control" id="Option022" name="datos_pasirinkimas2">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option023" name="tiekėjo_pasirinkimas2">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras2</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:orange;">
                <td></td>
                <td>Paslauga3</td>
                <td>  <select class="form-control" id="Option031" name="paslaugos_pasirinkimas3">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina3</td>
                <td>  <select class="form-control" id="Option032" name="datos_pasirinkimas3">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option033" name="tiekėjo_pasirinkimas3">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras3</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:orange;">
                <td></td>
                <td>Paslauga4</td>
                <td>  <select class="form-control" id="Option041" name="paslaugos_pasirinkimas4">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina4</td>
                <td>  <select class="form-control" id="Option042" name="datos_pasirinkimas4">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option043" name="tiekėjo_pasirinkimas4">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras4</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:yellow;">
                <td>Tyrimų Paslaugos</td>
                <td>Paslauga5</td>
                <td>  <select class="form-control" id="Option051" name="paslaugos_pasirinkimas5">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina5</td>
                <td>  <select class="form-control" id="Option052" name="datos_pasirinkimas5">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option053" name="tiekėjo_pasirinkimas5">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras5</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:yellow;">
                <td></td>
                <td>Paslauga6</td>
                <td>  <select class="form-control" id="Option061" name="paslaugos_pasirinkimas6">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina6</td>
                <td>  <select class="form-control" id="Option062" name="datos_pasirinkimas6">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option063" name="tiekėjo_pasirinkimas6">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras6</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:yellow;">
                <td></td>
                <td>Paslauga7</td>
                <td>  <select class="form-control" id="Option071" name="paslaugos_pasirinkimas7">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina7</td>
                <td>  <select class="form-control" id="Option072" name="datos_pasirinkimas7">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option073" name="tiekėjo_pasirinkimas7">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras7</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:yellow;">
                <td></td>
                <td>Paslauga8</td>
                <td>  <select class="form-control" id="Option081" name="paslaugos_pasirinkimas8">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina8</td>
                <td>  <select class="form-control" id="Option082" name="datos_pasirinkimas8">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option083" name="tiekėjo_pasirinkimas8">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras8</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:cyan;">
                <td>Pokalbiai/Interviu</td>
                <td>Paslauga9</td>
                <td>  <select class="form-control" id="Option091" name="paslaugos_pasirinkimas9">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina9</td>
                <td>  <select class="form-control" id="Option092" name="datos_pasirinkimas9">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option093" name="tiekėjo_pasirinkimas9">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras9</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:cyan;">
                <td></td>
                <td>Paslauga10</td>
                <td>  <select class="form-control" id="Option101" name="paslaugos_pasirinkimas10">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina10</td>
                <td>  <select class="form-control" id="Option102" name="datos_pasirinkimas10">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option103" name="tiekėjo_pasirinkimas10">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras10</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:cyan;">
                <td></td>
                <td>Paslauga11</td>
                <td>  <select class="form-control" id="Option111" name="paslaugos_pasirinkimas11">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina11</td>
                <td>  <select class="form-control" id="Option112" name="datos_pasirinkimas11">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option113" name="tiekėjo_pasirinkimas11">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras11</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:cyan;">
                <td></td>
                <td>Paslauga12</td>
                <td>  <select class="form-control" id="Option121" name="paslaugos_pasirinkimas12">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina12</td>
                <td>  <select class="form-control" id="Option122" name="datos_pasirinkimas12">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option123" name="tiekėjo_pasirinkimas12">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras12</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:lime;">
                <td>Firminiai Gaminiai</td>
                <td>Paslauga13</td>
                <td>  <select class="form-control" id="Option131" name="paslaugos_pasirinkimas13">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina13</td>
                <td>  <select class="form-control" id="Option132" name="datos_pasirinkimas13">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option133" name="tiekėjo_pasirinkimas13">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras13</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:lime;">
                <td></td>
                <td>Paslauga14</td>
                <td>  <select class="form-control" id="Option141" name="paslaugos_pasirinkimas14">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina14</td>
                <td>  <select class="form-control" id="Option142" name="datos_pasirinkimas14">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option143" name="tiekėjo_pasirinkimas14">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras14</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:lime;">
                <td></td>
                <td>Paslauga15</td>
                <td>  <select class="form-control" id="Option151" name="paslaugos_pasirinkimas15">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina15</td>
                <td>  <select class="form-control" id="Option152" name="datos_pasirinkimas15">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option153" name="tiekėjo_pasirinkimas15">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras15</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:lime;">
                <td></td>
                <td>Paslauga16</td>
                <td>  <select class="form-control" id="Option161" name="paslaugos_pasirinkimas16">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina16</td>
                <td>  <select class="form-control" id="Option162" name="datos_pasirinkimas16">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option163" name="tiekėjo_pasirinkimas16">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras16</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:magenta;">
                <td>Knygos</td>
                <td>Paslauga17</td>
                <td>  <select class="form-control" id="Option171" name="paslaugos_pasirinkimas17">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina17</td>
                <td>  <select class="form-control" id="Option172" name="datos_pasirinkimas17">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option173" name="tiekėjo_pasirinkimas17">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras17</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:magenta;">
                <td></td>
                <td>Paslauga18</td>
                <td>  <select class="form-control" id="Option181" name="paslaugos_pasirinkimas18">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina18</td>
                <td>  <select class="form-control" id="Option182" name="datos_pasirinkimas18">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option183" name="tiekėjo_pasirinkimas18">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras18</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:magenta;">
                <td></td>
                <td>Paslauga19</td>
                <td>  <select class="form-control" id="Option191" name="paslaugos_pasirinkimas19">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina19</td>
                <td>  <select class="form-control" id="Option192" name="datos_pasirinkimas19">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option193" name="tiekėjo_pasirinkimas19">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras19</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:magenta;">
                <td></td>
                <td>Paslauga20</td>
                <td>  <select class="form-control" id="Option201" name="paslaugos_pasirinkimas20">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina20</td>
                <td>  <select class="form-control" id="Option202" name="datos_pasirinkimas20">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option203" name="tiekėjo_pasirinkimas20">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras20</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:yellowgreen;">
                <td>Filmai</td>
                <td>Paslauga21</td>
                <td>  <select class="form-control" id="Option211" name="paslaugos_pasirinkimas21">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina21</td>
                <td>  <select class="form-control" id="Option212" name="datos_pasirinkimas21">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option213" name="tiekėjo_pasirinkimas21">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras21</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:yellowgreen;">
                <td></td>
                <td>Paslauga22</td>
                <td>  <select class="form-control" id="Option221" name="paslaugos_pasirinkimas22">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina22</td>
                <td>  <select class="form-control" id="Option222" name="datos_pasirinkimas22">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option223" name="tiekėjo_pasirinkimas22">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras22</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:yellowgreen;">
                <td></td>
                <td>Paslauga23</td>
                <td>  <select class="form-control" id="Option231" name="paslaugos_pasirinkimas23">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina23</td>
                <td>  <select class="form-control" id="Option232" name="datos_pasirinkimas23">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option233" name="tiekėjo_pasirinkimas23">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras23</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>
            <tr style="background-color:yellowgreen;">
                <td></td>
                <td>Paslauga24</td>
                <td>  <select class="form-control" id="Option241" name="paslaugos_pasirinkimas24">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td>Kaina24</td>
                <td>  <select class="form-control" id="Option242" name="datos_pasirinkimas24">
                        <option>Data1</option>
                        <option>Data2</option>
                        <option>Data3</option>
                    </select></td>
                <td>  <select class="form-control" id="Option243" name="tiekėjo_pasirinkimas24">
                        <option>Tiekėjas1</option>
                        <option>Tiekėjas2</option>
                        <option>Tiekėjas3</option>
                    </select></td>
                <td>Komentaras24</td>
                <td> <button type="button" class="btn btn-primary border">Pirkti</button></td>
            </tr>

            </tbody>
        </table>
    </div>

    <div class="container-fluid">
        <h1>Plačiau Apie Paslaugą</h1>
    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Paslauga


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <select class="form-control" id="Servise" name="Paslauga">
                <option>Paslauga1</option>
                <option>Paslauga2</option>
                <option>Paslauga3</option>
            </select>

        </div>

    </div>

    <div class="row">

        <div class="col-6" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightyellow;">

                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisi risus, varius at est non, suscipit ultricies odio. Ut volutpat condimentum ipsum, id sollicitudin felis congue at. Aliquam erat volutpat. Praesent vitae est quis metus facilisis tempor sed cursus nibh. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi vitae ullamcorper mauris. Nam sit amet sodales erat. Duis congue enim vel nibh hendrerit, eget porttitor odio finibus. Curabitur vehicula tellus ut nisi venenatis, in feugiat urna condimentum. Morbi in velit rhoncus, convallis felis eu, dignissim nulla. Sed porta sit amet velit consequat congue.


                </div>
            </div>

        </div>
        <div class="col-6" style="background-color:lavenderblush;">

            <img class="img-fluid" src="{{ asset('img/Paslauga1.png') }}" alt="Paslauga">

        </div>

    </div>

    <div class="row">

        <div class="col" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen">

                    Pirkta


                </div>
            </div>

        </div>
        <div class="col" style="background-color:lightyellow;">

            X

        </div>

        <div class="col border" style="background-color:lightgreen">

            Pardutoa


        </div>

        <div class="col" style="background-color:lightyellow;">

            Y

        </div>

    </div>

    <div class="d-flex">
        <div id="Rplace" class="p-2 bg-info flex-fill border">Vidutinis Įvertinimas</div>

        <div class="card-group flex-fill">
            <div class="card bg-primary">

                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=0 class="form-check-input" name="optradio">0/5</input>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=1 class="form-check-input" name="optradio">1/5</input>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=2 class="form-check-input" name="optradio">2/5</input>
                    </label>
                </div>

            </div>
            <div class="card bg-primary">

                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=3 class="form-check-input" name="optradio">3/5</input>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=4 class="form-check-input" name="optradio">4/5</input>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=5 class="form-check-input" name="optradio">5/5</input>
                    </label>
                </div>

            </div>
        </div>



        <button type="button" class="btn btn-primary border flex-fill" onclick="rate()">Įvertinti</button>
        <button type="button" class="btn btn-primary border flex-fill" onclick="Rerase()">Atšaukti Įvertinimą</button>
    </div>

    <div class="btn-group btn-block" style="max-height:100px">
        <button type="button" class="btn btn-primary border">Rodyti Komentarus</button>
        <button type="button" class="btn btn-primary border">Rašyti Komentarus</button>
    </div>

    <div class="container-fluid">
        <h1>Paslaugos Komentarai</h1>
    </div>
    <div class="row">
        <div class="col-sm-10">
            <div class="media border">
                <img src="{{ asset('img/Vartotojas1.png') }}" alt="Vartotojas" class="mr-3 mt-3 rounded-circle" style="width:60px;">
                <div class="media-body">
                    <h4>Vartotojas <small><i>Komentaro Pavadinimas</i></small></h4>
                    <p>Komentaro tekstas</p>
                </div>
            </div>
        </div>
        <div class="col-sm-2">

            <div class="btn-group-vertical btn-block">
                <button type="button" class="btn btn-primary border">Cituoti</button>
                <button type="button" class="btn btn-primary border">Įvertinti</button>
                <button type="button" class="btn btn-primary border">Pranešti apie pažeidimą</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10">
            <div class="media border">
                <img src="{{ asset('img/Vartotojas2.png') }}" alt="Vartotojas" class="mr-3 mt-3 rounded-circle" style="width:60px;">
                <div class="media-body">
                    <h4>Vartotojas <small><i>Komentaro Pavadinimas</i></small></h4>
                    <p>Komentaro tekstas</p>
                </div>
            </div>
        </div>
        <div class="col-sm-2">

            <div class="btn-group-vertical btn-block">
                <button type="button" class="btn btn-primary border">Cituoti</button>
                <button type="button" class="btn btn-primary border">Įvertinti</button>
                <button type="button" class="btn btn-primary border">Pranešti apie pažeidimą</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10">
            <div class="media border">
                <img src="{{ asset('img/Vartotojas3.png') }}" alt="Vartotojas" class="mr-3 mt-3 rounded-circle" style="width:60px;">
                <div class="media-body">
                    <h4>Vartotojas <small><i>Komentaro Pavadinimas</i></small></h4>
                    <p>Komentaro tekstas</p>
                </div>
            </div>
        </div>
        <div class="col-sm-2">

            <div class="btn-group-vertical btn-block">
                <button type="button" class="btn btn-primary border">Cituoti</button>
                <button type="button" class="btn btn-primary border">Įvertinti</button>
                <button type="button" class="btn btn-primary border">Pranešti apie pažeidimą</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-2">

            <img class="card-img-fluid" src="{{ asset('img/Vartotojas4.png') }}" alt="Vartotojas" style="max-width:100%">
            <div class="btn-group-vertical btn-block">
                <button type="button" class="btn btn-primary border">Spausdinti Komentarą</button>
                <button type="button" class="btn btn-primary border">Kopijuoti Komentarą</button>
                <button type="button" class="btn btn-primary border" onclick="Cerase();">Išvalyti Komentarą</button>
            </div>


        </div>
        <div class="col-sm-10">

            <div class="d-flex flex-wrap">
                <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addunderline();">Pabraukimas</button> </div>
                <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addbold();" >Paryškinimas</button> </div>
                <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="additalic()">Pasvyrimas</button> </div>
                <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addlower();">Apatinis Ind.</button> </div>
                <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addupper();"> Viršutinis Ind.</button> </div>
                <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border">Parinktis</button> </div>

                <div class="p-0 bg-warning">

                    <select class="form-control" id="fonts" name="sriftas" onchange="addfont('fonts');">
                        <option value=1>Timess New Roman</option>
                        <option value=2>Comic Sans MS</option>
                    </select>

                </div>
                <div class="p-0 bg-warning "> <input type="color" id="Icolor" value="#ff0000" size="35" onchange="addcolor('Icolor');">   </div>
                <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addhyperlink();">Hipersaitai</button> </div>
                <div class="p-0 bg-warning "><button type="button" id="Mpreview1" class="btn btn-primary border">Įkelti failą</button> </div>
                <div class="p-0 bg-warning ">

                    <select class="form-control" id="emoticons" name="sypseneles" onchange="addemoticon('emoticons')";>
                        <option>:D</option>
                        <option>:P</option>
                        <option>:)</option>
                    </select>

                </div>
                <div class="p-0 bg-warning"><button type="button" class="btn btn-primary border" onclick="preview();" data-toggle="modal" data-target="#myModal">Pirminė Vers.</button> </div>
            </div>

            <div class="form-group">
                <textarea class="form-control" rows="8" id="comment"></textarea>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <h1>Jau Užsakytos Paslaugos</h1>
    </div>

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Paslaugos Numeris</th>
                <th>Paslaugos Pavadinimas</th>
                <th>Paslaugos Pasirinkimai</th>
                <th>Užsisakymo Data</th>
                <th>Paslaugos Statusas</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>#1</td>
                <td>Paslauga1</td>
                <td>Pasirinkimai1</td>
                <td>Data1</td>
                <td>Statusas1</td>
                <td><button type="button" class="btn btn-primary border">Atšaukti</button></td>
            </tr>
            <tr>
                <td>#2</td>
                <td>Paslauga2</td>
                <td>Pasirinkimai2</td>
                <td>Data2</td>
                <td>Statusas2</td>
                <td><button type="button" class="btn btn-primary border">Atšaukti</button></td>
            </tr>
            <tr>
                <td>#3</td>
                <td>Paslauga3</td>
                <td>Pasirinkimai3</td>
                <td>Data3</td>
                <td>Statusas3</td>
                <td><button type="button" class="btn btn-primary border">Atšaukti</button></td>
            </tr>
            <tr>
                <td>#4</td>
                <td>Paslauga4</td>
                <td>Pasirinkimai4</td>
                <td>Data4</td>
                <td>Statusas4</td>
                <td><button type="button" class="btn btn-primary border">Atšaukti</button></td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection
@extends('modal')
