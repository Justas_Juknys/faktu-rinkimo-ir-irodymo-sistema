@extends('layout')
@section('title')
    Paieška
    @endsection
@section('javascript')
    <script type="text/javascript">

        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });

        function addIR()
        {
            $(comment).surroundSelectedText("&&", "/&");
        }

        function addARBA()
        {
            $(comment).surroundSelectedText("%%", "/%");
        }

        function addNE()
        {
            $(comment).surroundSelectedText("--", "/-");
        }


        function Cswitch1(B){
            var x;
            switch (B)
            {
                case 'Bvideo':
                x = 'vinfo';
                break;
                case 'Bimportant':
                x = 'itag';
                break;
                case 'Bunread':
                x = 'nread';
                break;
                case 'Bnew':
                x = 'ncom'
                break;
                case 'Bcomments':
                x = 'ucom';
                break;
                case 'Bold':
                x = 'oinfo';
                break;
            }
            var e = document.getElementById(B).style.backgroundColor
            if (e == "green")
            {
                document.getElementById(B).style.backgroundColor="orange";
                document.getElementById(B).style.color="black";
                document.getElementById(x).value="1";
            } else if(e=="orange")
            {
                document.getElementById(B).style.backgroundColor="red";
                document.getElementById(B).style.color="white";
                document.getElementById(x).value="0";
            } else if(e=="red")
            {
                document.getElementById(B).style.backgroundColor="green";
                document.getElementById(x).value="2";
            }
        }


        function Cswitch2(B)
        {
            var x;
            var y;
            var z;
            if (B == "Ball")
            {
                x = "Bsome";
                y = "Bnone";
                z = '2';
            } else if (B == "Bsome")
            {
                x = "Ball";
                y = "Bnone";
                z = '1';
            } else{
                x = "Ball";
                y = "Bsome";
                z = '0';
            }
            document.getElementById(x).style.backgroundColor="darkcyan";
            document.getElementById(y).style.backgroundColor="darkcyan";
            document.getElementById(B).style.backgroundColor="blue";
            document.getElementById('N_O_A').value=z;

        }
    </script>
@endsection
@section('main')
    <form method="POST" action="paie_ka" style="margin-button: 1em;">
        {{csrf_field()}}

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <button type="submit" class="btn btn-primary btn-block"><p class="jautru">Ieškoti</p></button>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <div class="form-group">
                <textarea class="form-control" rows="1" id="comment" name="search_text" placeholder="Įveskite Užklausą" style="max-width:100%;"></textarea>
            </div>

        </div>

    </div>







    <div class="row">

        <div class="col border" style="background-color:lightgreen;">

            Loginiai Operatoriai

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <div class="btn-group btn-block" style="max-height:100px">
                <button type="button" class="btn btn-primary border" onclick="addIR()">IR</button>
                <button type="button" class="btn btn-primary border" onclick="addARBA()">ARBA</button>
                <button type="button" class="btn btn-primary border" onclick="addNE()">NE</button>
            </div>
        </div>


        <div class="col border" style="background-color:lightgreen;">

            Ieškoma

        </div>

        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="iesk1" name="search_target">
                <option>Blogo Įrašų</option>
                <option>Faktų Įrašų</option>
                <option>Abiejų</option>
            </select>

        </div>

    </div>



    <div class="row">

        <div class="col-9 border" style="background-color:lightgreen;">

            Papildomos Parinktys (Tik Blogo Įrašų Paieškai)

        </div>
        <div class="col-3" style="background-color:lavenderblush;">

            <button type="button" class="btn btn-primary btn-block border"><p class="jautru">Slėpti</p></button>


        </div>
    </div>


    <div class="row">

        <div class="col border" style="background-color:lightgreen;">

            Rūšiavimas

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Stype" name="sort_method">
                <option>Data</option>
                <option>Populiarumas</option>
                <option>Komentarų Skaičius</option>
                <option>Įvertinimas</option>
                <option>Socialinių Platformų Pasidalinimai</option>
                <option>Įrašo Dydis</option>
                <option>Informacijos Kiekis</option>
            </select>


        </div>

        <div class="col border" style="background-color:lightgreen;">

            Įrašo Autorius

        </div>

        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="SName" name="author_name">
                <option>Vardas1</option>
                <option>Vardas2</option>
                <option>Vardas3</option>
            </select>


        </div>


    </div>






    <div class="row">

        <div class="col border" style="background-color:lightgreen;">

            Rūšiavimo Tipas

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Sorder" name="sort_type">
                <option>Didėjančiai</option>
                <option>Mažėjančiai</option>
            </select>


        </div>

        <div class="col border" style="background-color:lightgreen;">

            Informacijos Šaltinis

        </div>

        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Ssource" name="search_source">
                <option>Šaltinis1</option>
                <option>Šaltinis2</option>
                <option>Šaltinis3</option>
            </select>


        </div>


    </div>



    <div class="row">

        <div class="col border" style="background-color:lightgreen;">

            Nuo

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Syear1" name="from_year">
                <option>2019</option>
            </select>


        </div>

        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Smonth1" name="from_month">
                <option>Sausis</option>
                <option>Vasaris</option>
                <option>Kovas</option>
                <option>Balandis</option>
                <option>Gegužė</option>
                <option>Birželis</option>
                <option>Liepa</option>
                <option>Rugpjūtis</option>
                <option>Rugsėjis</option>
                <option>Spalis</option>
                <option>Lapkritis</option>
                <option>Gruodis</option>
            </select>

        </div>

        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Sday1" name="from_day">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
                <option>11</option>
                <option>12</option>
                <option>13</option>
                <option>14</option>
                <option>15</option>
                <option>16</option>
                <option>17</option>
                <option>18</option>
                <option>19</option>
                <option>20</option>
                <option>21</option>
                <option>22</option>
                <option>23</option>
                <option>24</option>
                <option>25</option>
                <option>26</option>
                <option>27</option>
                <option>28</option>
                <option>29</option>
                <option>30</option>
                <option>31</option>
            </select>


        </div>


    </div>


    <div class="row">

        <div class="col border" style="background-color:lightgreen;">

            Iki

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Syear2" name="to_year">
                <option>2019</option>
            </select>


        </div>

        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Smonth2" name="to_month">
                <option>Sausis</option>
                <option>Vasaris</option>
                <option>Kovas</option>
                <option>Balandis</option>
                <option>Gegužė</option>
                <option>Birželis</option>
                <option>Liepa</option>
                <option>Rugpjūtis</option>
                <option>Rugsėjis</option>
                <option>Spalis</option>
                <option>Lapkritis</option>
                <option>Gruodis</option>
            </select>

        </div>

        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Sday2" name="to_day">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
                <option>11</option>
                <option>12</option>
                <option>13</option>
                <option>14</option>
                <option>15</option>
                <option>16</option>
                <option>17</option>
                <option>18</option>
                <option>19</option>
                <option>20</option>
                <option>21</option>
                <option>22</option>
                <option>23</option>
                <option>24</option>
                <option>25</option>
                <option>26</option>
                <option>27</option>
                <option>28</option>
                <option>29</option>
                <option>30</option>
                <option>31</option>
            </select>


        </div>


    </div>



    <div class="row">

        <div class="col border" style="background-color:lightgreen;">

            Vartotojų Srauto Ribos

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <textarea class="form-control" rows="1" id="Ucount1" name="user_amount_from">Nuo</textarea>


        </div>

        <div class="col" style="background-color:lavenderblush;">

            <textarea class="form-control" rows="1" id="Ucount2" name="user_amount_to">Iki</textarea>

        </div>

    </div>


    <div class="row">

        <div class="col border" style="background-color:lightgreen;">

            Blogo Įrašo apimtis (žodžiais)

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <textarea class="form-control" rows="1" id="Tcount1" name="blog_size_from">Nuo</textarea>


        </div>

        <div class="col" style="background-color:lavenderblush;">

            <textarea class="form-control" rows="1" id="Tcount2" name="blog_size_to">Iki</textarea>

        </div>

    </div>







    <div class="row">

        <div class="col border" style="background-color:lightgreen;">

            Iliustracijų kiekis

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <textarea class="form-control" rows="1" id="Tcount1" name="illustration_amount_from">Nuo</textarea>


        </div>

        <div class="col" style="background-color:lavenderblush;">

            <textarea class="form-control" rows="1" id="Tcount2" name="illustration_amount_to">Iki</textarea>

        </div>


    </div>



    <div class="d-flex flex-wrap align-content-stretch">
        <button id="Bvideo" type="button" class="btn btn-primary border" data-toggle="tooltip" data-placement="bottom" title="Turi vaizdo įrašų arba nuotraukų" style="background-color:green;" onclick="Cswitch1('Bvideo')" >Vaizdinė Informacija</button>
        <button id="Bimportant" type="button" class="btn btn-primary border" data-toggle="tooltip" data-placement="bottom" title="Įrašai, kuriuos esate įtraukę į svarbių įrašų sąrašą" style="background-color:orange;" onclick="Cswitch1('Bimportant')">Svarbūs</button>
        <button id="Bunread" type="button" class="btn btn-primary border" data-toggle="tooltip" data-placement="bottom" title="Jūsų dar neperžiūrėti įrašai"  style="background-color:orange;" onclick="Cswitch1('Bunread')">Dar Neskaityti</button>
        <button id="Bnew" type="button" class="btn btn-primary border" style="background-color:orange;" data-toggle="tooltip" data-placement="bottom" title="Įrašai su bent vienu nauju komentaru" onclick="Cswitch1('Bnew')">Nauji Komentarai</button>
        <button type="button" class="btn btn-primary border" >Įrašyti Parinkimus</button>
        <button type="button" class="btn btn-primary border">Naudoti Įrašytus Parinkimus</button>
        <button id="Bcomments" type="button" class="btn btn-primary border" data-toggle="tooltip" data-placement="bottom" title="Įrašai, kuriuos esate komentave" style="background-color:orange;" onclick="Cswitch1('Bcomments')">Su Vartotojo Komentarais</button>
        <button id="Bold" type="button" class="btn btn-primary border" data-toggle="tooltip" data-placement="bottom" title="Įtraukti senas įrašų versijas" style="background-color:red;" onclick="Cswitch1('Bold')">Įtraukti Pasenusios Informacijos Įrašus</button>
    </div>

        <input type="hidden" id="vinfo" name="video_info" value="2">
        <input type="hidden" id="itag" name="important_tag" value="1">
        <input type="hidden" id="nread" name="not_read" value="1">
        <input type="hidden" id="ncom" name="new_comments" value="1">
        <input type="hidden" id="ucom" name="user_comments" value="1">
        <input type="hidden" id="oinfo" name="old_info" value="0">












    <div class="row">

        <div class="col-9 border" style="background-color:lightgreen;">

            Kategorijos

        </div>
        <div class="col-3" style="background-color:lavenderblush;">

            <button type="button" class="btn btn-primary btn-block border"><p class="jautru">Slėpti</p></button>


        </div>
    </div>
@foreach($categories as $category)
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" name="cat{{$category->category_id}}" value="{{$category->category_id}}">{{$category->category_title}}
        </label>
    </div>
        @endforeach




    <div class="btn-group btn-block">
        <button id="Ball" type="button" class="btn btn-primary border" style="background-color:darkcyan" onclick="Cswitch2('Ball')">Visi</button>
        <button id="Bsome" type="button" class="btn btn-primary border" style="background-color:blue" onclick="Cswitch2('Bsome')">Bent Vienas</button>
        <button id="Bnone" type="button" class="btn btn-primary border" style="background-color:darkcyan" onclick="Cswitch2('Bnone')">Nei Vienas</button>

    </div>
        <input type="hidden" id="N_O_A" name="none_or_and" value="1">
    </form>


    <div class="row">
        <div class="col border" style="background-color:lightgreen;">

            Rezultatai


        </div>
    </div>
@isset ($blogposts)
@foreach($blogposts as $blogpost)
    <div class="row">
        <div class="col border" style="background-color:lightgreen;">

            {{$blogpost->blog_topic}}


        </div>

        <div class="col border" style="background-color:lightyellow;">

            {{$blogpost->authors->user_nickname}}


        </div>
        <div class="col border" style="background-color:lightyellow;">

            {{$blogpost->created_at}}


        </div>
        <div class="col border" style="background-color:lightyellow;">

            {{$blogpost->blog_category_id}} kategorija


        </div>
        <div class="col border" style="background-color:lightyellow;">

            Subkategorijos


        </div>
        <div class="col border" style="background-color:lightyellow;">

            Informacijos Šaltiniai


        </div>

    </div>
    @endforeach
@endisset
    @if(empty($blogposts))

    <div class="row">
        <div class="col border" style="background-color:orange;">

            Įspėjimas Jeigu Nebuvo Rasta Tinkamų Rezultatų


        </div>
    </div>

    <div class="row">
        <div class="col border" style="background-color:lightgreen;">

            Rekomenduojami Pataisymai


        </div>

        <div class="col border" style="background-color:lightyellow;">

            Pataisymas1


        </div>
        <div class="col border" style="background-color:lightyellow;">

            Pataisymas2


        </div>
        <div class="col border" style="background-color:lightyellow;">

            Pataisymas3


        </div>
        <div class="col border" style="background-color:lightyellow;">

            Pataisymas4


        </div>
        <div class="col border" style="background-color:lightyellow;">

            Pataisymas5


        </div>

    </div>
    @endif
    @endsection
