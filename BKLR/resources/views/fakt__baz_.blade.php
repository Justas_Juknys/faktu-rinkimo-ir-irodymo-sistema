@extends('layout')
@section('title')
    Faktų Bazė
    @endsection
@section('javascript')
    <script type="text/javascript">
        function addIR() {
            $(comment).surroundSelectedText("&&", "/&");
        }

        function addARBA() {
            $(comment).surroundSelectedText("%%", "/%");
        }

        function addNE() {
            $(comment).surroundSelectedText("--", "/-");
        }


        function Cswitch1(B){
            var e = document.getElementById(B).style.backgroundColor
            if (e == "green"){
                document.getElementById(B).style.backgroundColor="orange";
                document.getElementById(B).style.color="black";
            } else if(e=="orange"){
                document.getElementById(B).style.backgroundColor="red";
                document.getElementById(B).style.color="white";
            } else if(e=="red") {document.getElementById(B).style.backgroundColor="green";}
        }
    </script>
    @endsection
@section('main')
    <div class="row">

        <div class="col border" style="background-color:lightgreen;">

            Peržiūros Tipas

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Otype" name="perziurostipas">
                <option>Kategorinis</option>
                <option>Naujasi Faktai</option>
                <option>Naujausi Papildymai</option>
                <option>Paieška</option>
            </select>


        </div>

        <div class="col border" style="background-color:lightgreen;">

            Rezultatų Forma

        </div>

        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Rform" name="rezultatuforma">
                <option>Faktai (Paprasta Forma)</option>
                <option>Faktai (Išsami Forma)</option>
                <option>Teorija (Paprasta Forma)</option>
                <option>Teorija (Išsami Forma)</option>
            </select>


        </div>


    </div>






    <div class="row">
        <div class="col border" style="background-color:lightgreen;">

            Kategorijos


        </div>
    </div>



    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" value="">Kategorija1
        </label>
    </div>
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" value="">Kategorija2
        </label>
    </div>
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" value="">Kategorija3
        </label>
    </div>
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" value="">Kategorija4
        </label>
    </div>
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" value="">Kategorija5
        </label>
    </div>
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" value="">Kategorija6
        </label>
    </div>
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" value="">Kategorija7
        </label>
    </div>
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" value="">Kategorija8
        </label>
    </div>
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" value="">Kategorija9
        </label>
    </div>
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" value="">Kategorija10
        </label>
    </div>
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" value="">Kategorija11
        </label>
    </div>
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" value="">Kategorija12
        </label>
    </div>
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" value="">Kategorija13
        </label>
    </div>
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" value="">Kategorija14
        </label>
    </div>
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" value="">Kategorija15
        </label>
    </div>
    <div class="form-check-inline">
        <label class="form-check-label">
            <input type="checkbox" class="form-check-input" value="">Kategorija16
        </label>
    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <button type="button" class="btn btn-primary btn-block"><p class="jautru">Ieškoti</p></button>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <div class="form-group">
                <textarea class="form-control" rows="1" id="comment" style="max-width:100%;">Įveskite Užklausą</textarea>
            </div>

        </div>

    </div>

    <div class="row">

        <div class="col border" style="background-color:lightgreen;">

            Loginiai Operatoriai

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <div class="btn-group btn-block" style="max-height:100px">
                <button type="button" class="btn btn-primary border" onclick="addIR()">IR</button>
                <button type="button" class="btn btn-primary border" onclick="addARBA()">ARBA</button>
                <button type="button" class="btn btn-primary border" onclick="addNE()">NE</button>
            </div>
        </div>



    </div>

    <div class="row">

        <div class="col border" style="background-color:lightgreen;">

            Rūšiavimo Tipas

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Stype" name="Rūšiavimotipas">
                <option>Data</option>
                <option>Nuorodų Į Įrašus Kiekis</option>
                <option>Informacijos Šaltinių Kiekis</option>
            </select>


        </div>

        <div class="col border" style="background-color:lightgreen;">

            Rūšiavimo Tvarka

        </div>

        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Sorder" name="rūšiavimotvarka">
                <option>Didėjančiai</option>
                <option>Mažėjančiai</option>
            </select>


        </div>


    </div>


    <div class="row">

        <div class="col border" style="background-color:lightgreen;">

            Fakto Tipas

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Ftype" name="faktotipas">
                <option>Įrodymas</option>
                <option>Pozytivi Prielaida</option>
                <option>Nesusijęs Faktas</option>
                <option>Negatyvi Prielaida</option>
                <option>Paneigimas</option>
            </select>


        </div>

        <div class="col" style="background-color:levander;">

            <div class="btn-group btn-block" style="max-height:100px">
                <button id="Bnfacts" type="button" class="btn btn-primary border" style="background-color:orange;" onclick="Cswitch1('Bnfacts')">Nauji Faktai</button>
                <button id="Bufacts" type="button" class="btn btn-primary border" style="background-color:orange;" onclick="Cswitch1('Bufacts')">Papildyti Faktai</button>
            </div>

        </div>

    </div>

    <div class="row">

        <div class="col border" style="background-color:lightgreen;">

            Teorijos Statusas

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Ttype" name="teorijostipas">
                <option>Įrodyta</option>
                <option>Dalinai Įrodyta</option>
                <option>Dalis Paneigta, Dalis Įrodyta</option>
                <option>Dalinai Paneigta</option>
                <option>Paneigta</option>
            </select>


        </div>

        <div class="col" style="background-color:levander;">

            <div class="btn-group btn-block" style="max-height:100px">
                <button id="Bntheories" type="button" class="btn btn-primary border" style="background-color:orange;" onclick="Cswitch1('Bntheories')">Naujos Teorijos</button>
                <button id="Butheories" type="button" class="btn btn-primary border" style="background-color:orange;" onclick="Cswitch1('Butheories')">Papildytos Teorijos</button>
            </div>

        </div>

    </div>

    <div class="row">
        <div class="col border" style="background-color:cyan;">

            Kur Ieškoti


        </div>
    </div>

    <div class="btn-group btn-block" style="max-height:100px">
        <button id="Btdescription" type="button" class="btn btn-primary border" style="background-color:green;" onclick="Cswitch1('Btdescription')">Teorijos Apibrėžimas</button>
        <button id="Btclaim" type="button" class="btn btn-primary border" style="background-color:green;" onclick="Cswitch1('Btclaim')">Teorijos Teiginys</button>
        <button id="Bevidence" type="button" class="btn btn-primary border" style="background-color:green;" onclick="Cswitch1('Bevidence')">Originalus Įrodymas</button>
        <button id="Bfact" type="button" class="btn btn-primary border" style="background-color:green;" onclick="Cswitch1('Bfact')">Faktas</button>
    </div>

    <div class="row">

        <div class="col-3 border" style="background-color:lightgreen;">

            Teorijos Rodymas

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <select class="form-control" id="Tnegative" name="teigiamos_neigiamos_teoriju_rodymas">
                <option>Teorija Ir Jai Priešinga Teorija Rodomos Atskyrai</option>
                <option>Teorija ir Jai Priešinga Teorija Rodomos Kartu</option>
                <option>Nerodyti Priešingų Teorijų</option>
                <option>Rodyti Tik Priešingas Teorijas</option>
            </select>

        </div>

    </div>

    <div class="row">
        <div class="col border" style="background-color:lightgreen;">

            Rasti Faktai


        </div>
    </div>


    <div class="row">
        <div class="col border" style="background-color:lightblue;">

            Paprastas Faktas


        </div>
        <div class="col border" style="background-color:lightblue;">

            Fakto Data


        </div>
        <div class="col border" style="background-color:lightblue;">

            Fakto Tipas


        </div>
    </div>

    <div class="row">
        <div class="col border" style="background-color:lightyellow;">

            Faktas Phasellus nunc lectus, fermentum quis finibus eget, condimentum ac ex. Phasellus molestie eros sed lorem gravida placerat. Duis eu enim et est accumsan facilisis.


        </div>
    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <button type="button" class="btn btn-primary btn-block"><p class="jautru">Eiti Į</p></button>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <select class="form-control" id="Bselect1" name="blogopasirinkimas1">
                <option>Blogo Įrašas1</option>
                <option>Blogo Įrašas2</option>
                <option>Blogo Įrašas3</option>
            </select>

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <button type="button" class="btn btn-primary btn-block"><p class="jautru">Eiti Į</p></button>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <select class="form-control" id="Sselect1" name="šaltiniopasirinkimas1">
                <option>Informacijos Šaltinis1</option>
                <option>Informacijos Šaltinis2</option>
                <option>Informacijos Šaltinis3</option>
            </select>

        </div>

    </div>

    <div class="row">
        <div class="col border" style="background-color:lightblue;">

            Išpėstas Faktas


        </div>
        <div class="col border" style="background-color:lightblue;">

            Fakto Data


        </div>
        <div class="col border" style="background-color:lightblue;">

            Fakto Tipas


        </div>
        <div class="col border" style="background-color:lightblue;">

            Kategorija1


        </div>
        <div class="col border" style="background-color:lightblue;">

            Kategorija2


        </div>
        <div class="col border" style="background-color:lightblue;">

            Kategorija3


        </div>
    </div>

    <div class="row">
        <div class="col border" style="background-color:lightyellow;">

            Faktas/Teorija Morbi porta consequat scelerisque. Aliquam accumsan sem mauris, sit amet molestie est eleifend rutrum. Praesent tellus enim, scelerisque placerat tempor eget, ullamcorper a purus. Aenean sollicitudin lacinia felis, vitae tristique tellus mollis nec.


        </div>
    </div>


    <div class="row">
        <div class="col border" style="background-color:lightyellow;">

            Faktas Phasellus nunc lectus, fermentum quis finibus eget, condimentum ac ex. Phasellus molestie eros sed lorem gravida placerat. Duis eu enim et est accumsan facilisis.


        </div>
    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <button type="button" class="btn btn-primary btn-block"><p class="jautru">Eiti Į</p></button>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <select class="form-control" id="Bselect2" name="blogopasirinkimas2">
                <option>Blogo Įrašas1</option>
                <option>Blogo Įrašas2</option>
                <option>Blogo Įrašas3</option>
            </select>

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <button type="button" class="btn btn-primary btn-block"><p class="jautru">Eiti Į</p></button>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <select class="form-control" id="Sselect2" name="šaltiniopasirinkimas2">
                <option>Informacijos Šaltinis1</option>
                <option>Informacijos Šaltinis2</option>
                <option>Informacijos Šaltinis3</option>
            </select>

        </div>

    </div>


    <div class="row">
        <div class="col border" style="background-color:lightblue;">

            Paprasta Teorija


        </div>
        <div class="col border" style="background-color:lightblue;">

            Teorijos Data


        </div>
        <div class="col border" style="background-color:lightblue;">

            Teorijos Statusas


        </div>
        <div class="col border" style="background-color:lightblue;">

            Kategorija1


        </div>
        <div class="col border" style="background-color:lightblue;">

            Kategorija2


        </div>
        <div class="col border" style="background-color:lightblue;">

            Kategorija3


        </div>
    </div>

    <div class="row">
        <div class="col border" style="background-color:lightyellow;">

            Faktas/Teorija Morbi porta consequat scelerisque. Aliquam accumsan sem mauris, sit amet molestie est eleifend rutrum. Praesent tellus enim, scelerisque placerat tempor eget, ullamcorper a purus. Aenean sollicitudin lacinia felis, vitae tristique tellus mollis nec.


        </div>
    </div>



    <div class="row">
        <div class="col-2 border">
            <div class="row">
                <div class="col border">
                    <div class="row">
                        <div class="col border" style="background-color:lightgreen;height:100px;">

                            <p class="jautru">Faktas 1</p>


                        </div>
                    </div>
                    <div class="row">
                        <div class="col border" style="background-color:lightgreen;height:100px;">

                            <p class="jautru">Fakto Tipas</p>


                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-10 border" style="background-color:lightyellow;">

            Faktas Phasellus nunc lectus, fermentum quis finibus eget, condimentum ac ex. Phasellus molestie eros sed lorem gravida placerat. Duis eu enim et est accumsan facilisis.


        </div>

    </div>

    <div class="row">
        <div class="col-2 border">
            <div class="row">
                <div class="col border">
                    <div class="row">
                        <div class="col border" style="background-color:lightgreen;height:100px;">

                            <p class="jautru">Faktas 2</p>


                        </div>
                    </div>
                    <div class="row">
                        <div class="col border" style="background-color:lightgreen;height:100px;">

                            <p class="jautru">Fakto Tipas</p>


                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-10 border" style="background-color:lightyellow;">

            Faktas Phasellus nunc lectus, fermentum quis finibus eget, condimentum ac ex. Phasellus molestie eros sed lorem gravida placerat. Duis eu enim et est accumsan facilisis.


        </div>

    </div>

    <div class="row">

        <div class="col-3 border" style="background-color:lightgreen;">

            Pasirinkti Ieškomą Faktą

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <select class="form-control" id="Fselect3" name="faktopasirinkimas3">
                <option>Faktas1</option>
                <option>Faktas2</option>
                <option>Faktas3</option>
            </select>

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <button type="button" class="btn btn-primary btn-block"><p class="jautru">Eiti Į</p></button>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <select class="form-control" id="Bselect3" name="blogopasirinkimas3">
                <option>Blogo Įrašas1</option>
                <option>Blogo Įrašas2</option>
                <option>Blogo Įrašas3</option>
            </select>

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <button type="button" class="btn btn-primary btn-block"><p class="jautru">Eiti Į</p></button>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <select class="form-control" id="Sselect3" name="šaltiniopasirinkimas3">
                <option>Informacijos Šaltinis1</option>
                <option>Informacijos Šaltinis2</option>
                <option>Informacijos Šaltinis3</option>
            </select>

        </div>

    </div>

    <div class="row">
        <div class="col border" style="background-color:lightblue;">

            Išplėsta Teorija


        </div>
        <div class="col border" style="background-color:lightblue;">

            Teorijos Data


        </div>
        <div class="col border" style="background-color:lightblue;">

            Teorijos Statusas


        </div>
        <div class="col border" style="background-color:lightblue;">

            Kategorija1


        </div>
        <div class="col border" style="background-color:lightblue;">

            Kategorija2


        </div>
        <div class="col border" style="background-color:lightblue;">

            Kategorija3


        </div>
    </div>

    <div class="row">
        <div class="col border" style="background-color:lightyellow;">

            Teorija Morbi porta consequat scelerisque. Aliquam accumsan sem mauris, sit amet molestie est eleifend rutrum. Praesent tellus enim, scelerisque placerat tempor eget, ullamcorper a purus. Aenean sollicitudin lacinia felis, vitae tristique tellus mollis nec.


        </div>
    </div>

    <div class="row">
        <div class="col border" style="background-color:purple;min-height:10px;">

        </div>
    </div>

    <div class="row">
        <div class="col-6 border">

            <div class="row">
                <div class="col-2 border">
                    <div class="row">
                        <div class="col border">
                            <div class="row">
                                <div class="col border" style="background-color:lightgreen;height:100px;">

                                    <p class="jautru">Teiginys 1</p>


                                </div>
                            </div>
                            <div class="row">
                                <div class="col border" style="background-color:lightgreen;height:100px;">

                                    <p class="jautru">Teiginio Tipas</p>


                                </div>
                            </div>

                            <div class="row">
                                <div class="col border" style="background-color:lightgreen;height:100px;">

                                    <p class="jautru">Teiginio Autorius</p>


                                </div>
                            </div>

                            <div class="row">
                                <div class="col border" style="background-color:lightgreen;height:100px;">

                                    <p class="jautru">Teiginio Statusas</p>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="col-10 border" style="background-color:lightyellow;">

                    Faktas Phasellus nunc lectus, fermentum quis finibus eget, condimentum ac ex. Phasellus molestie eros sed lorem gravida placerat. Duis eu enim et est accumsan facilisis.


                </div>

            </div>









            <div class="row">
                <div class="col-2 border">
                    <div class="row">
                        <div class="col border">
                            <div class="row">
                                <div class="col border" style="background-color:lightgreen;height:100px;">

                                    <p class="jautru">Originalus Įrodymas</p>


                                </div>
                            </div>
                            <div class="row">
                                <div class="col border" style="background-color:lightgreen;height:100px;">

                                    <p class="jautru">Įrodymo Statusas</p>


                                </div>
                            </div>




                        </div>
                    </div>
                </div>


                <div class="col-10 border" style="background-color:lightyellow;">

                    Teginio Paaiškinimas1 Phasellus nunc lectus, fermentum quis finibus eget, condimentum ac ex. Phasellus molestie eros sed lorem gravida placerat. Duis eu enim et est accumsan facilisis.


                </div>

            </div>

            <div class="row">
                <div class="col-2 border">
                    <div class="row">
                        <div class="col border">
                            <div class="row">
                                <div class="col border" style="background-color:lightgreen;height:100px;">

                                    <p class="jautru">Originalus Įrodymas 1</p>


                                </div>
                            </div>
                            <div class="row">
                                <div class="col border" style="background-color:lightgreen;height:100px;">

                                    <p class="jautru">Įrodymo Statusas</p>


                                </div>
                            </div>




                        </div>
                    </div>
                </div>


                <div class="col-10 border" style="background-color:lightyellow;">

                    Teginio Paaiškinimas2 Phasellus nunc lectus, fermentum quis finibus eget, condimentum ac ex. Phasellus molestie eros sed lorem gravida placerat. Duis eu enim et est accumsan facilisis.


                </div>

            </div>

        </div>

        <div class="col-6 border">











            <div class="row">
                <div class="col-2 border">
                    <div class="row">
                        <div class="col border">
                            <div class="row">
                                <div class="col border" style="background-color:lightgreen;height:100px;">

                                    <p class="jautru">Faktas 1</p>


                                </div>
                            </div>
                            <div class="row">
                                <div class="col border" style="background-color:lightgreen;height:100px;">

                                    <p class="jautru">Fakto Tipas</p>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col border" style="background-color:lightgreen;height:100px;">

                                    <p class="jautru">Fakto Data</p>


                                </div>
                            </div>
                            <div class="row">
                                <div class="col border" style="background-color:lightgreen;height:100px;">

                                    <p class="jautru">Įrodęs Asmuo</p>


                                </div>
                            </div>




                        </div>
                    </div>
                </div>


                <div class="col-10 border" style="background-color:lightyellow;">

                    Faktas Phasellus nunc lectus, fermentum quis finibus eget, condimentum ac ex. Phasellus molestie eros sed lorem gravida placerat. Duis eu enim et est accumsan facilisis.


                </div>

            </div>

            <div class="row">
                <div class="col-2 border">
                    <div class="row">
                        <div class="col border">

                            <div class="row">
                                <div class="col border" style="background-color:lightgreen;height:100px;">

                                    <p class="jautru">Faktas 2</p>


                                </div>
                            </div>
                            <div class="row">
                                <div class="col border" style="background-color:lightgreen;height:100px;">

                                    <p class="jautru">Fakto Tipas</p>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col border" style="background-color:lightgreen;height:100px;">

                                    <p class="jautru">Fakto Data</p>


                                </div>
                            </div>
                            <div class="row">
                                <div class="col border" style="background-color:lightgreen;height:100px;">

                                    <p class="jautru">Įrodęs Asmuo</p>


                                </div>
                            </div>




                        </div>
                    </div>
                </div>


                <div class="col-10 border" style="background-color:lightyellow;">

                    Faktas Phasellus nunc lectus, fermentum quis finibus eget, condimentum ac ex. Phasellus molestie eros sed lorem gravida placerat. Duis eu enim et est accumsan facilisis.


                </div>

            </div>

        </div>



    </div>















    <div class="row">

        <div class="col-3 border" style="background-color:lightgreen;">

            Ko Ieškoma

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <select class="form-control" id="Select4" name="pasirinkimas4">
                <option>Faktų</option>
                <option>Originalių Įrodymų</option>
                <option>Pačios Teorijos</option>
            </select>

        </div>

    </div>

    <div class="row">

        <div class="col border" style="background-color:lightgreen;">

            Teiginys

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Eselect41" name="teiginioparinkimas41">
                <option>Teiginys1</option>
                <option>Teiginys2</option>
                <option>Teiginys3</option>
            </select>


        </div>

        <div class="col border" style="background-color:lightgreen;">

            Faktas

        </div>

        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Fselect41" name="faktoparinkimas41">
                <option>Faktas1</option>
                <option>Faktas2</option>
                <option>Faktas3</option>
            </select>


        </div>


    </div>

    <div class="row">

        <div class="col border" style="background-color:lightgreen;">

            Teiginys

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Eselect42" name="teiginioparinkimas42">
                <option>Teiginys1</option>
                <option>Teiginys2</option>
                <option>Teiginys3</option>
            </select>


        </div>

        <div class="col border" style="background-color:lightgreen;">

            Įrodymas

        </div>

        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Pselect42" name="įrodymoparinkimas42">
                <option>Įrodymas1</option>
                <option>Įrodymas2</option>
                <option>Įrodymas3</option>
            </select>


        </div>


    </div>


    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <button type="button" class="btn btn-primary btn-block"><p class="jautru">Eiti Į</p></button>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <select class="form-control" id="Bselect4" name="blogopasirinkimas4">
                <option>Blogo Įrašas1</option>
                <option>Blogo Įrašas2</option>
                <option>Blogo Įrašas3</option>
            </select>

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <button type="button" class="btn btn-primary btn-block"><p class="jautru">Eiti Į</p></button>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <select class="form-control" id="Sselect4" name="šaltiniopasirinkimas4">
                <option>Informacijos Šaltinis1</option>
                <option>Informacijos Šaltinis2</option>
                <option>Informacijos Šaltinis3</option>
            </select>

        </div>

    </div>

























    @endsection
