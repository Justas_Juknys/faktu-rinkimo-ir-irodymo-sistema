@extends('layout')
@section('main')
    <form method="POST" action="/BKLR/public/subkategorijos" style="margin-button: 1em;">
        {{csrf_field()}}
    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Subkategorijos Pavadinimas


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <input type="text" class = "form-control" name = "subcategory_title" >

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Subkategorijos Aprašymas


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <input type="text" class = "form-control" name = "subcategory_description" >

        </div>

    </div>

        <input type="hidden" value="{{$req}}" name="category_id">

    <button type="submit" class="btn btn-primary btn-block border">Pridėti Subkategoriją</button>
    </form>

    @endsection
