@extends('layout')
@section('main')
    <div class="row">

        <div class="col" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightblue;">

                    Subkategorijos


                </div>
            </div>

        </div>

    </div>
    <form method="POST" action="/BKLR/public/papildoma_kategorija" style="margin-button: 1em;">
        @csrf
        @foreach($subcategories as $subcategory)
            <div class="form-check-inline">
                <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" name="sub{{$subcategory->subcategory_id}}" value="{{$subcategory->subcategory_id}}">{{$subcategory->subcategory_title}}
                </label>
            </div>
        @endforeach
        <input type="hidden" name="blog_id" value="{{$blog_id}}">
        <input type="hidden" name="subcategory_max_id" value="{{$subcategory_max_id}}">
            <button type="submit" class="btn btn-primary btn-block border">Pridėti Subkategorijas</button>
    </form>
    @endsection
