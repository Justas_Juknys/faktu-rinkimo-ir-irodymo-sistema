@extends('layout')
@section('title')
    Pagrindinis Puslapis
    @endsection
@section('main')
    <div class="container">
        <h1>Trumpas Svetainės Aprašas</h1>
        <div class="d-flex">
            <div class="p-2 bg-info flex-fill">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisi risus, varius at est non, suscipit ultricies odio. Ut volutpat condimentum ipsum, id sollicitudin felis congue at. Aliquam erat volutpat. Praesent vitae est quis metus facilisis tempor sed cursus nibh. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi vitae ullamcorper mauris. Nam sit amet sodales erat. Duis congue enim vel nibh hendrerit, eget porttitor odio finibus. Curabitur vehicula tellus ut nisi venenatis, in feugiat urna condimentum. Morbi in velit rhoncus, convallis felis eu, dignissim nulla. Sed porta sit amet velit consequat congue. Quisque accumsan nibh ultricies turpis placerat, a maximus purus interdum. Mauris justo lorem, iaculis sit amet pharetra vel, accumsan eu sapien. Quisque viverra purus ac elit condimentum egestas.</div>
        </div>
        <h1>Naujausi Blogo Įrašai</h1>
        <div class=row>
            <div class="col" style="background-color:lavender;"><div class="p-2 bg-info flex-fill" style="min-height:250px">Etiam pellentesque, lectus eget tristique efficitur, neque odio egestas diam, et molestie libero urna eu dolor. Nam eget rhoncus quam. Nullam id felis venenatis, laoreet justo in, mattis nunc. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</div>

                <button type="button" class="btn btn-primary  btn-block">Skaityti Plačiau</button>
            </div>
            <div class="col" style="background-color:lavender;"><div class="p-2 bg-warning flex-fill" style="min-height:250px">Phasellus nunc lectus, fermentum quis finibus eget, condimentum ac ex. Phasellus molestie eros sed lorem gravida placerat. Duis eu enim et est accumsan facilisis.</div>

                <button type="button" class="btn btn-primary btn-block">Skaityti Plačiau</button>
            </div>
            <div class="col" style="background-color:lavender;">  <div class="p-2 bg-primary flex-fill" style="min-height:250px">Morbi porta consequat scelerisque. Aliquam accumsan sem mauris, sit amet molestie est eleifend rutrum. Praesent tellus enim, scelerisque placerat tempor eget, ullamcorper a purus. Aenean sollicitudin lacinia felis, vitae tristique tellus mollis nec.</div>

                <button type="button" class="btn btn-primary btn-block">Skaityti Plačiau</button>

            </div>
        </div>
        <h1>Svetainės Istorija</h1>
        <div class="p-2 bg-info flex-fill">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisi risus, varius at est non, suscipit ultricies odio. Ut volutpat condimentum ipsum, id sollicitudin felis congue at. Aliquam erat volutpat. Praesent vitae est quis metus facilisis tempor sed cursus nibh. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi vitae ullamcorper mauris. Nam sit amet sodales erat. Duis congue enim vel nibh hendrerit, eget porttitor odio finibus. Curabitur vehicula tellus ut nisi venenatis, in feugiat urna condimentum. Morbi in velit rhoncus, convallis felis eu, dignissim nulla. Sed porta sit amet velit consequat congue. Quisque accumsan nibh ultricies turpis placerat, a maximus purus interdum. Mauris justo lorem, iaculis sit amet pharetra vel, accumsan eu sapien. Quisque viverra purus ac elit condimentum egestas.</div>
    </div>
    <h1>Autoriaus Biografija</h1>
    <div class="card-group">
        <div class="card" style="width:400px">
            <div class="card-body">
                <h4 class="card-title">Justas Juknys</h4>
                <p class="card-text">Žmogaus aprašymas</p>
            </div>
        </div>
        <div class="card" style="width:400px">

            <img class="card-img-top-fluid" src="{{ asset('img/Veidas1.png') }}" alt="Card image">

        </div>
    </div>
    <h1>Nuorodos Į Kitas Svetaines</h1>
    <img class="img-fluid" src="{{ asset('img/Nuoroda1.png') }}" alt="Nuoroda" style="max-width:30%">
    <img class="img-fluid" src="{{ asset('img/Nuoroda1.png') }}" alt="Nuoroda" style="max-width:30%">
    <img class="img-fluid" src="{{ asset('img/Nuoroda1.png') }}" alt="Nuoroda" style="max-width:30%">
    @endsection
