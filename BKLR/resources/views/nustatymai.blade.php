@extends('layout')
@section('title')
    Nustatymai
    @endsection
@section('main')
    <div class="row">

        <div class="col" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Spalvų Schema


                </div>
            </div>

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Scheme" name="schema">
                <option>Schema1</option>
                <option>Schema2</option>
                <option>Schema3</option>
            </select>

        </div>

    </div>

    <div class="row">

        <div class="col" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Viešasis Profilis


                </div>
            </div>

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <div class="form-check">
                <input type="checkbox" class="form-check-input" value="">
            </div>

        </div>

    </div>

    <div class="row">

        <div class="col" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Rekomenduojami Puslapiai


                </div>
            </div>

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <div class="form-check">
                <input type="checkbox" class="form-check-input" value="">
            </div>

        </div>

    </div>

    <div class="row">

        <div class="col" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Slaptažodžio Įsiminimas


                </div>
            </div>

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <div class="form-check">
                <input type="checkbox" class="form-check-input" value="">
            </div>

        </div>

    </div>

    <div class="row">

        <div class="col" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Blogo Įrašų Dydis


                </div>
            </div>

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Bsize" name="blogo_įrašų_dydis">
                <option>8</option>
                <option>9</option>
                <option>10</option>
                <option>11</option>
                <option>12</option>
                <option>13</option>
                <option>14</option>
                <option>15</option>
                <option>16</option>
                <option>17</option>
                <option>18</option>
                <option>19</option>
                <option>20</option>
                <option>21</option>
                <option>22</option>
                <option>23</option>
                <option>24</option>
            </select>

        </div>

    </div>

    <div class="row">

        <div class="col" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Vienu Metu Pakraunama Blogo Įrašų


                </div>
            </div>

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Bamount" name="blogo_įrašų_kiekis">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
            </select>

        </div>

    </div>

    <div class="row">

        <div class="col" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Maksimalus Rezultatų Kiekis Paieškos Rezultatų Puslapyje


                </div>
            </div>

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Ramount" name="paieškos_rezultatų_kiekis">
                <option>30</option>
                <option>35</option>
                <option>40</option>
                <option>45</option>
                <option>50</option>
                <option>55</option>
                <option>60</option>
                <option>65</option>
                <option>70</option>
                <option>75</option>
            </select>

        </div>

    </div>

    <div class="row">

        <div class="col" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Automatinis Komentarų Rodymas


                </div>
            </div>

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <div class="form-check">
                <input type="checkbox" class="form-check-input" value="">
            </div>

        </div>

    </div>

    <div class="row">

        <div class="col" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Komentarų Rodomas Kiekis Viename Puslapyje


                </div>
            </div>

        </div>
        <div class="col" style="background-color:lavenderblush;">

            <select class="form-control" id="Camount" name="komentarų_puslapyje_kiekis">
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
                <option>11</option>
                <option>12</option>
                <option>13</option>
                <option>14</option>
                <option>15</option>
            </select>

        </div>

    </div>
@endsection
