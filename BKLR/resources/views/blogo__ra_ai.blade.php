@extends('layout')
@section('title')
    Blogo Įrašai
    @endsection
@section('javascript')
    <script type="text/javascript">
        function rate()
        {
            var a = $('input[name=optradio]:checked').val();
            var fieldNameElement = document.getElementById('Rplace').innerText;
            document.getElementById('Rplace').innerHTML = a;
        }

        function Rerase()
        {
            $('input[name=optradio]').prop('checked',false);
            document.getElementById('Rplace').innerHTML = "Įvertinimas";
        }

        @include('CommentFunc')
    </script>
    @section('javascript')
@section('main')
    <div class="d-flex">
    <div class="p-2 mr-auto bg-info border">{{$newest->blog_topic}}</div>
    <button type="button" class="btn btn-primary border">Įtraukti Į kategoriją</button>
    </div>
    <div class="d-flex">
        <button type="button" class="btn btn-primary border">Naujesnis Įrašas</button>
        <button type="button" class="btn btn-primary border">Senesnis Įrašas</button>
        <div class="p-2 bg-info flex-fill border">{{$newest->authors->user_nickname}}</div>
        <div class="p-2 bg-info flex-fill border"><b>{{$newest->categories->category_title}}</b></div>
    </div>
    @for($i=0;$i<$used_category_amount;$i++)
    <div class="d-flex">
        <div class="p-2 bg-info border">{{$category_names[$i]}}</div>
        <div class="p-2 bg-primary flex-grow-1 border">
            @foreach(unserialize($subcategory_name_sets[$i]) as $subcategory_name)
                {{$subcategory_name}}
                @endforeach
        </div>
    </div>
    @endfor
    <div class="d-flex">
        <div class="p-2 bg-warning flex-grow-1 border">Įspėjimas, jei informacija jau pasenusi</div>
    </div>
    <div class="card-group">
        <div class="card bg-primary">
            <div class="card-body text-center">
                <p class="card-text">{{$newest->blog_text}}</p>
            </div>
        </div>
        <div class="card bg-primary">

            <img class="card-img-fluid" src="{{ asset('img/Iliustracija1.png') }}" alt="Iliustracija" style="max-width:100%">
            <img class="card-img-fluid" src="{{ asset('img/Iliustracija2.png') }}" alt="Iliustracija" style="max-width:100%">
            <img class="card-img-fluid" src="{{ asset('img/Iliustracija3.png') }}" alt="Iliustracija" style="max-width:100%">

        </div>
    </div>

    <h1>Daugiau Šia Tema</h1>
    <img class="card-img-fluid" src="{{ asset('img/Srekomendacija1.png') }}" alt="Iliustracija" style="max-width:30%">
    <img class="card-img-fluid" src="{{ asset('img/Srekomendacija2.png') }}" alt="Iliustracija" style="max-width:30%">
    <img class="card-img-fluid" src="{{ asset('img/Srekomendacija3.png') }}" alt="Iliustracija" style="max-width:30%">

    <div class="d-flex">

        <div id="Rplace" class="p-2 bg-info flex-fill border">Įvertinimas</div>


        <div class="card-group flex-fill">
            <div class="card bg-primary">

                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=0 class="form-check-input" name="optradio">0/5</input>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=1 class="form-check-input" name="optradio">1/5</input>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=2 class="form-check-input" name="optradio">2/5</input>
                    </label>
                </div>

            </div>
            <div class="card bg-primary">

                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=3 class="form-check-input" name="optradio">3/5</input>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=4 class="form-check-input" name="optradio">4/5</input>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=5 class="form-check-input" name="optradio">5/5</input>
                    </label>
                </div>

            </div>
        </div>



        <button type="button" class="btn btn-primary border flex-fill" onclick="rate()">Įvertinti</button>
        <button type="button" class="btn btn-primary border flex-fill" onclick="Rerase()">Atšaukti Įvertinimą</button>
    </div>
    <h1>Socialinių Svetainių Nuorodos</h1>
    <img class="card-img-fluid" src="{{ asset('img/SNuoroda1.png') }}" alt="SNuoroda" style="max-width:18%">
    <img class="card-img-fluid" src="{{ asset('img/SNuoroda2.png') }}" alt="SNuoroda" style="max-width:18%">
    <img class="card-img-fluid" src="{{ asset('img/SNuoroda3.png') }}" alt="SNuoroda" style="max-width:18%">
    <img class="card-img-fluid" src="{{ asset('img/SNuoroda4.png') }}" alt="SNuoroda" style="max-width:18%">
    <img class="card-img-fluid" src="{{ asset('img/SNuoroda5.png') }}" alt="SNuoroda" style="max-width:18%">
    <h1>Įrašo Komentarai</h1>


    <div class="row">
        <div class="col-sm-10">
            <div class="media border">
                <img src="{{ asset('img/Vartotojas1.png') }}" alt="Vartotojas" class="mr-3 mt-3 rounded-circle" style="width:60px;">
                <div class="media-body">
                    <h4>Vartotojas <small><i>Komentaro Pavadinimas</i></small></h4>
                    <p>Komentaro tekstas</p>
                </div>
            </div>
        </div>
        <div class="col-sm-2">

            <div class="btn-group-vertical btn-block">
                <button type="button" class="btn btn-primary border">Cituoti</button>
                <button type="button" class="btn btn-primary border">Įvertinti</button>
                <button type="button" class="btn btn-primary border">Pranešti apie pažeidimą</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10">
            <div class="media border">
                <img src="{{ asset('img/Vartotojas2.png') }}" alt="Vartotojas" class="mr-3 mt-3 rounded-circle" style="width:60px;">
                <div class="media-body">
                    <h4>Vartotojas <small><i>Komentaro Pavadinimas</i></small></h4>
                    <p>Komentaro tekstas</p>
                </div>
            </div>
        </div>
        <div class="col-sm-2">

            <div class="btn-group-vertical btn-block">
                <button type="button" class="btn btn-primary border">Cituoti</button>
                <button type="button" class="btn btn-primary border">Įvertinti</button>
                <button type="button" class="btn btn-primary border">Pranešti apie pažeidimą</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10">
            <div class="media border">
                <img src="{{ asset('img/Vartotojas3.png') }}" alt="Vartotojas" class="mr-3 mt-3 rounded-circle" style="width:60px;">
                <div class="media-body">
                    <h4>Vartotojas <small><i>Komentaro Pavadinimas</i></small></h4>
                    <p>Komentaro tekstas</p>
                </div>
            </div>
        </div>
        <div class="col-sm-2">

            <div class="btn-group-vertical btn-block">
                <button type="button" class="btn btn-primary border">Cituoti</button>
                <button type="button" class="btn btn-primary border">Įvertinti</button>
                <button type="button" class="btn btn-primary border">Pranešti apie pažeidimą</button>
            </div>
        </div>
    </div>




        @include('comment_writing')




@include('modal')

    @endsection
