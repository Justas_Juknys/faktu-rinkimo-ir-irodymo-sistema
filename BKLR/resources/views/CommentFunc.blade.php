function addbold() {
$(comment).surroundSelectedText("<b>", "</b>");
}

function additalic() {
$(comment).surroundSelectedText("<i>", "</i>");
}

function addunderline() {
$(comment).surroundSelectedText("<ins>", "</ins>");
}

function addlower() {
$(comment).surroundSelectedText("<sub>", "</sub>");
}

function addupper() {
$(comment).surroundSelectedText("<sup>", "</sup>");
}

function addhyperlink() {
$(comment).surroundSelectedText("<a href=\"www.jusuadresas.com\">", "</a>");
}

function Cerase()
{
document.getElementById('comment').value = "";
}

function addemoticon(E)
{
var e = document.getElementById(E);
$(comment).surroundSelectedText("", e.options[e.selectedIndex].text);
}

function addcolor(E)
{
var e = document.getElementById(E).value;
var color = "<font color=" + e + ">";
    $(comment).surroundSelectedText(color,"</font>");
}

function addfont(E)
{
var e = document.getElementById(E).value;
if (e==1) $(comment).surroundSelectedText("<nobr class=a>","</nobr>");
else if (e==2) $(comment).surroundSelectedText("<nobr class=c>","</nobr>");
}
