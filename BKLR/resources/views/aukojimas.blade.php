@extends('layout')
@section('title')
    Aukojimas
    @endsection
@section('main')

    <div class="container-fluid">
        <h1>Aukų Naudojimas</h1>
    </div>
    <div class="row">

        <div class="col border" style="background-color:lightgreen;">


            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisi risus, varius at est non, suscipit ultricies odio. Ut volutpat condimentum ipsum, id sollicitudin felis congue at. Aliquam erat volutpat. Praesent vitae est quis metus facilisis tempor sed cursus nibh. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi vitae ullamcorper mauris. Nam sit amet sodales erat. Duis congue enim vel nibh hendrerit, eget porttitor odio finibus. Curabitur vehicula tellus ut nisi venenatis, in feugiat urna condimentum. Morbi in velit rhoncus, convallis felis eu, dignissim nulla. Sed porta sit amet velit consequat congue. Quisque accumsan nibh ultricies turpis placerat, a maximus purus interdum. Mauris justo lorem, iaculis sit amet pharetra vel, accumsan eu sapien. Quisque viverra purus ac elit condimentum egestas.


        </div>
    </div>
    <img class="img-fluid" src="{{ asset('img/iliustracija1.png') }}" alt="iliustracija" style="max-width:30%">
    <img class="img-fluid" src="{{ asset('img/iliustracija2.png') }}" alt="iliustracija" style="max-width:30%">
    <img class="img-fluid" src="{{ asset('img/iliustracija3.png') }}" alt="iliustracija" style="max-width:30%">
    <div class="container-fluid">
        <h1>Aukoti Galima Šiose Svetainėse</h1>
    </div>
    <div class="row">

        <div class="col border" style="background-color:lightgreen;">
            <img class="img-fluid" src="{{ asset('img/nuotrauka1.png') }}" alt="nuotrauka" style="max-width:100%">

            <div class="container-fluid">
                Trumpas Aprašymas
            </div>
            <button type="button" class="btn btn-primary btn-block border">Aukoti</button>

        </div>
        <div class="col border" style="background-color:lightgreen;">
            <img class="img-fluid" src="{{ asset('img/nuotrauka2.png') }}" alt="nuotrauka" style="max-width:100%">

            <div class="container-fluid">
                Trumpas Aprašymas
            </div>
            <button type="button" class="btn btn-primary btn-block border">Aukoti</button>

        </div>
        <div class="col border" style="background-color:lightgreen;">
            <img class="img-fluid" src="{{ asset('img/nuotrauka3.png') }}" alt="nuotrauka" style="max-width:100%">

            <div class="container-fluid">
                Trumpas Aprašymas
            </div>
            <button type="button" class="btn btn-primary btn-block border">Aukoti</button>

        </div>
    </div>
    <div class="container-fluid">
        <h1>Aukų Statistika</h1>
    </div>

    <div class="table-responsive">
        <table class="table-bordered" style="background-color:lightyellow;">
            <thead style="background-color:violet;">
            <tr>
                <th></th>
                <th>Aukojimo Svetainė1</th>
                <th>Aukojimo Svetainė2</th>
                <th>Aukojimo Svetainė3</th>
                <th>Bendra Statistika</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td style="background-color:violet;">Surinkta Aukų</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td style="background-color:violet;">Išleista Aukų</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td style="background-color:violet;">Didžiausia Auka</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td style="background-color:violet;">Aukotojų Skaičius</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection
