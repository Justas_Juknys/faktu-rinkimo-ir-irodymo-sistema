@extends('layout')
@section('title')
    Taisyklės
    @endsection
@section('main')
    <div class="container-fluid">
        <h1>Trumpa Taisyklių Versija</h1>
    </div>
    <div class="d-flex">
        <div class="p-2 bg-info flex-fill">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin convallis mauris est, vel mollis dolor tempus et. Nullam sodales sem nibh, tincidunt commodo nisi consequat viverra. Curabitur a felis semper, elementum sem sit amet, auctor tellus. Vivamus at iaculis urna, non lacinia diam. Vestibulum fermentum purus et consequat facilisis. Etiam nec molestie eros, sed facilisis mi. Pellentesque semper sagittis sem nec hendrerit. Praesent felis mauris, malesuada eget fermentum consequat, imperdiet a odio. Fusce lobortis lobortis fermentum.



            Aliquam malesuada metus in ligula sagittis gravida. Nulla in vehicula velit. Vestibulum lacinia sapien lorem, eget elementum nulla ornare non. Sed ornare sed libero ut feugiat. Maecenas rutrum luctus nisi sed vulputate. Nullam eget pellentesque metus. Praesent sed dignissim lacus.



            Proin aliquet tempus magna et mattis. Pellentesque sit amet est in turpis euismod malesuada vitae quis quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce suscipit accumsan sagittis. Pellentesque at dignissim ex. Donec nec turpis a sem aliquam feugiat elementum non enim. Nulla eget vehicula orci. Proin ac iaculis lacus. Phasellus hendrerit justo nisi. Donec iaculis mollis interdum. Proin scelerisque dolor vitae justo eleifend fermentum.</div>
    </div>
    <div class="container-fluid">
        <h1>Pilnos Taisyklės</h1>
    </div>
    <div class="d-flex">
        <div class="p-2 bg-info flex-fill">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vel semper ipsum. Sed tristique nunc at purus suscipit imperdiet. Pellentesque ligula augue, luctus nec ex sed, accumsan suscipit lacus. Curabitur vestibulum porttitor felis, eu mollis dolor egestas eu. Nullam in erat a quam euismod placerat quis a sapien. Ut eget risus vestibulum, interdum ipsum sit amet, faucibus neque. Proin euismod est in tortor tempor, a pharetra elit eleifend. Maecenas quis purus sed leo sodales efficitur eu a mauris. Proin id urna mi. Proin ligula lacus, fringilla vitae augue lacinia, commodo porttitor orci.



            Etiam ac lorem at purus rutrum scelerisque. Pellentesque faucibus egestas urna. Morbi aliquam sed diam maximus aliquet. Praesent iaculis, quam eget vulputate commodo, justo urna tincidunt nisl, sit amet consequat tortor ipsum nec purus. Fusce volutpat mollis arcu, at condimentum nunc congue in. Suspendisse sit amet luctus lectus. Duis eu leo tortor. In augue felis, rhoncus eu tempor sit amet, cursus sed arcu. Curabitur lacinia lacinia molestie. Praesent id lacus felis. Etiam gravida vitae nibh non sagittis. Donec felis elit, cursus et vulputate et, euismod egestas ipsum. Pellentesque malesuada lorem vel orci bibendum mollis.



            Etiam bibendum tincidunt congue. Proin ut commodo augue. Suspendisse potenti. Praesent imperdiet justo eu sapien elementum lobortis. Vivamus vitae turpis eu metus sollicitudin tristique nec et est. Vivamus metus mi, sollicitudin pretium posuere nec, facilisis eget dolor. Maecenas commodo sem magna, imperdiet tincidunt nibh consequat non. Vivamus sit amet arcu elit. Fusce cursus dui ut lacus laoreet semper.



            Suspendisse viverra condimentum massa, in posuere ante venenatis nec. Aliquam ullamcorper sagittis velit id egestas. Pellentesque in nibh a diam sagittis maximus vitae in eros. Aenean faucibus placerat viverra. Etiam lacinia luctus leo, id pellentesque elit cursus non. Fusce venenatis orci purus, sit amet vestibulum ante interdum non. Nullam enim lacus, gravida nec feugiat sit amet, semper ac neque. Morbi malesuada mi eu rutrum sollicitudin. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce efficitur nunc nec leo rutrum, ac bibendum arcu egestas. Nunc dictum id mauris sollicitudin tincidunt. Suspendisse dolor metus, ultrices vel tempor at, sollicitudin vitae tellus. Maecenas euismod fermentum ex et faucibus. Nam dignissim ultrices scelerisque.



            Etiam iaculis pulvinar hendrerit. Curabitur vel urna odio. Donec aliquet tellus sit amet est efficitur, nec tristique metus congue. Nulla ornare libero vel enim gravida, vel lacinia sapien ornare. Nunc ut euismod justo, congue tempus odio. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis maximus pellentesque dui, aliquam aliquam erat euismod quis. Integer id augue eget tortor sollicitudin tempor sit amet eu velit. Nulla arcu ex, porta quis enim ac, vehicula ornare sem. Suspendisse cursus ac lacus vitae porttitor. Sed viverra tortor eget nisi facilisis, vitae pulvinar tortor mollis.



            Quisque sit amet euismod ipsum. Mauris ultricies, leo nec malesuada porta, erat lorem pharetra ex, rhoncus commodo arcu felis sed orci. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce consectetur a eros ac fermentum. In quis metus in felis gravida scelerisque. Sed imperdiet facilisis erat, eget ultrices nisl imperdiet eu. Praesent facilisis erat sapien. Sed vel mollis velit. Praesent mauris metus, elementum ac arcu ac, facilisis dictum libero.



            Nullam sodales odio ac porta scelerisque. Donec laoreet tortor tempor mauris rutrum bibendum. Aliquam sollicitudin magna sed semper vulputate. Quisque in purus pulvinar, blandit felis at, maximus lacus. Etiam lacinia facilisis elit a volutpat. Pellentesque consequat est nec vestibulum egestas. Morbi ac ligula quis mi efficitur rhoncus. Nunc rhoncus quam eu ullamcorper volutpat. Donec fringilla a odio vel lacinia. Nam ac magna quis ipsum mollis tincidunt. Ut nec nisl tortor.



            Ut odio ipsum, interdum quis dignissim in, consequat ut purus. Ut lacinia maximus nibh, eu lobortis turpis pulvinar id. Mauris dignissim erat tellus, ac commodo erat euismod nec. Praesent velit tellus, ultricies quis ex cursus, ultrices varius ipsum. Etiam eu dui eget lectus luctus mattis non vel ante. In non semper neque. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur eget libero tristique nulla consequat fermentum a eget ipsum. Vestibulum a accumsan ante. Donec blandit ut enim at lacinia. Fusce ultrices sollicitudin vulputate. Aliquam id ante enim. Donec lectus lectus, interdum et turpis vel, porttitor ultricies eros.



            Cras mattis eleifend elit in ullamcorper. Nulla ac mauris id eros molestie consectetur. Phasellus egestas vitae nisi eget rutrum. Donec blandit, massa non maximus eleifend, purus purus consectetur lectus, vel commodo ligula turpis eu libero. Quisque semper, est auctor venenatis porttitor, odio sem facilisis augue, in aliquam augue nibh eget risus. Suspendisse condimentum, turpis id tristique luctus, augue nibh ultrices ligula, dignissim venenatis nibh lectus ac lorem. Sed lorem ante, vulputate vel ante mattis, bibendum consectetur sem. Phasellus sed risus finibus, dignissim turpis ut, vehicula mi. Suspendisse sed luctus dolor. Nullam pharetra dui dui, sed tempus diam varius eget. Sed accumsan, ex sed rhoncus condimentum, velit augue viverra nisi, in accumsan nibh ex ac odio. Sed semper porttitor turpis, vel posuere ipsum aliquet quis. Curabitur sodales et erat vel finibus.



            Praesent volutpat mi vitae ipsum semper consequat. Quisque consequat libero velit, a mollis lectus placerat quis. Fusce finibus quam eu justo suscipit scelerisque. Suspendisse a vehicula odio, vitae lobortis diam. Quisque ut felis risus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam faucibus massa quis elit condimentum, vel posuere metus commodo. Proin a molestie mi.</div>
    </div>
    <div class="container-fluid">
        <h1>Autorinių Teisių Taisyklės</h1>
    </div>
    <div class="d-flex">
        <div class="p-2 bg-info flex-fill">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin convallis mauris est, vel mollis dolor tempus et. Nullam sodales sem nibh, tincidunt commodo nisi consequat viverra. Curabitur a felis semper, elementum sem sit amet, auctor tellus. Vivamus at iaculis urna, non lacinia diam. Vestibulum fermentum purus et consequat facilisis. Etiam nec molestie eros, sed facilisis mi. Pellentesque semper sagittis sem nec hendrerit. Praesent felis mauris, malesuada eget fermentum consequat, imperdiet a odio. Fusce lobortis lobortis fermentum.



            Aliquam malesuada metus in ligula sagittis gravida. Nulla in vehicula velit. Vestibulum lacinia sapien lorem, eget elementum nulla ornare non. Sed ornare sed libero ut feugiat. Maecenas rutrum luctus nisi sed vulputate. Nullam eget pellentesque metus. Praesent sed dignissim lacus.



            Proin aliquet tempus magna et mattis. Pellentesque sit amet est in turpis euismod malesuada vitae quis quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce suscipit accumsan sagittis. Pellentesque at dignissim ex. Donec nec turpis a sem aliquam feugiat elementum non enim. Nulla eget vehicula orci. Proin ac iaculis lacus. Phasellus hendrerit justo nisi. Donec iaculis mollis interdum. Proin scelerisque dolor vitae justo eleifend fermentum.</div>
    </div>
    @endsection
