<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/textsize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style.css3">
    <script type="text/javascript" src="js/bootstrap.bundle.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script src="js/rangyinputs-jquery-src.js"></script>

    @yield('javascript')
</head>
<body>





<nav class="navbar navbar-expand-sm bg-light navbar-light">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" >
        <span class="navbar-toggler-icon"></span>
    </button>



    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">

            <li class="nav-item active">
                <a class="nav-link" href="/BKLR/public/pagrindinis_puslapis">Pagrindinis Puslapis</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/BKLR/public/blogo__ra_ai">Blogo Įrašai</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/BKLR/public/turinys">Turinys</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/BKLR/public/paie_ka">Paieška</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/BKLR/public/fakt__baz_">Faktų Bazė</a>
            </li>
            @if (session('user_registered')!='1')
            <li class="nav-item">
                <a class="nav-link" href="/BKLR/public/registracija">Registracija</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/BKLR/public/prisijungimas">Prisijungimas</a>
            </li>
            @else
            <li class="nav-item">
                <a class="nav-link" href="/BKLR/public/nustatymai">Nustatymai</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/BKLR/public/vartotojo_s_saja">Vartotojo Sąsaja</a>
            </li>
                <li class="nav-item">
                    <a class="nav-link" href="/BKLR/public/prisijungimas/baigti">Atsijungti</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/BKLR/public/blogo__ra_o_ra_ymas">Rašyti Blogo Įrašą</a>
                </li>
                @endif



        </ul>

        <div class="nav-item dropdown" >
            <a class="nav-link dropdown-toggle my-2 my-sm-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:DimGrey">
                Informacijos peržiūra
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="/BKLR/public/kategorijos">kategorijos</a>
                <a class="dropdown-item" href="/BKLR/public/nemokamos_paslaugos">Nemokamos Paslaugos</a>
                <a class="dropdown-item" href="/BKLR/public/aukojimas">Aukojimas</a>
                <a class="dropdown-item" href="/BKLR/public/taisykl_s">Taisyklės</a>
                <a class="dropdown-item" href="/BKLR/public/kontaktai">Kontaktai</a>
            </div>
        </div>

        <div class="nav-item dropdown" >
            <a class="nav-link dropdown-toggle my-2 my-sm-0" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:DimGrey">
                Papildomi Funckionalumai
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="/BKLR/public/mokamos_paslaugos">Mokamos Paslaugos</a>
                <a class="dropdown-item" href="/BKLR/public/nemokamos_paslaugos">Nemokamos Paslaugos</a>
                <a class="dropdown-item" href="/BKLR/public/aukojimas">Aukojimas</a>
                <a class="dropdown-item" href="/BKLR/public/taisykl_s">Taisyklės</a>
                <a class="dropdown-item" href="/BKLR/public/kontaktai">Kontaktai</a>
            </div>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-2" style="background-color:lavender;">

            <div class="p-2 bg-info flex-fill"><p class="jautru">Rekomenduojami Puslapiai<p></div>
            <img class="img-fluid" src="{{ asset('img/Rekomendacija1.png') }}" alt="Rekomendacija">
            <img class="img-fluid" src="{{ asset('img/Rekomendacija2.png') }}" alt="Rekomendacija">
            <img class="img-fluid" src="{{ asset('img/Rekomendacija3.png') }}" alt="Rekomendacija">
            <div class="p-2 bg-info flex-fill"><p class="jautru">Reklama</p></div>
            <img class="img-fluid" src="{{ asset('img/Rkm1.png') }}" alt="Reklama">
            <img class="img-fluid" src="{{ asset('img/Rkm2.png') }}" alt="Reklama">
            <img class="img-fluid" src="{{ asset('img/Rkm3.png') }}" alt="Reklama">
            <img class="img-fluid" src="{{ asset('img/Rkm4.png') }}" alt="Reklama">
            <img class="img-fluid" src="{{ asset('img/Rkm5.png') }}" alt="Reklama">
            <img class="img-fluid" src="{{ asset('img/Rkm6.png') }}" alt="Reklama">

        </div>


        <div class="col-8" style="background-color:lavenderblush;">
@yield('main')
        </div>


        <div class="col-2" style="background-color:lavender;">

            <div class="p-2 bg-info flex-fill"><p class="jautru">Greitoji Navigacija<p></div>
            <div class="btn-group-vertical btn-block">
                <button type="button" class="btn btn-primary btn-block"><p class="jautru">Tema 1<p></button>
                <button type="button" class="btn btn-primary btn-block"><p class="jautru">Tema 2<p></button>
                <button type="button" class="btn btn-primary btn-block"><p class="jautru">Tema 3<p></button>
                <button type="button" class="btn btn-primary btn-block"><p class="jautru">Tema 4<p></button>
                <button type="button" class="btn btn-primary btn-block"><p class="jautru">Tema 5<p></button>
                <button type="button" class="btn btn-primary btn-block"><p class="jautru">Tema 6<p></button>
                <button type="button" class="btn btn-primary btn-block"><p class="jautru">Tema 7<p></button>
                <button type="button" class="btn btn-primary btn-block"><p class="jautru">Tema 8<p></button>
                <button type="button" class="btn btn-primary btn-block"><p class="jautru">Tema 9<p></button>
                <button type="button" class="btn btn-primary btn-block"><p class="jautru">Tema 10<p></button>
                <button type="button" class="btn btn-primary btn-block"><p class="jautru">Tema 11<p></button>
                <button type="button" class="btn btn-primary btn-block"><p class="jautru">Tema 12<p></button>
                <button type="button" class="btn btn-primary btn-block"><p class="jautru">Tema 13<p></button>
                <button type="button" class="btn btn-primary btn-block"><p class="jautru">Tema 14<p></button>
                <button type="button" class="btn btn-primary btn-block"><p class="jautru">Tema 15<p></button>
                <button type="button" class="btn btn-primary btn-block"><p class="jautru">Tema 16<p></button>
            </div>

        </div>
    </div>
</div>

<div class="jumbotron">
    <p class="text-center">Visos autorinės teisės saugomos</p>
    <p class="text-center">© 2019</p>
</div>

@yield('modal')

</body>
</html>
