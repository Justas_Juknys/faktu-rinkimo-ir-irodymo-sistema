@extends('layout')
@section('title')
    Įrašo rašymas
    @endsection
@section('javascript')
    <script type="text/javascript">

        @include('CommentFunc')
    </script>

    @endsection
@section('main')
    <form method="POST" action="blogo__ra_ai/create" style="margin-button: 1em;">
    @csrf
        <div class="row">

            <div class="col-3" style="background-color:lavenderblush;">

                <div class="row">
                    <div class="col border" style="background-color:lightgreen;">

                        Įrašo pavadinimas


                    </div>
                </div>

            </div>
            <div class="col-9" style="background-color:lavenderblush;">

                <input type="text" class = "form-control" name = "blog_topic" >

            </div>

        </div>
        @include('blog_post_writing')
        <input type="hidden" name="blog_author_id" value="{{session('userid')}}">

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Blogo Kategorija


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <select name="blog_category_id" class = "form-control">
                @foreach ($categories as $category)
                <option value={{$category->category_id}}>{{$category->category_title}}</option>
                    @endforeach
            </select>
        </div>

    </div>

        <div class="row">

            <div class="col-3" style="background-color:lavenderblush;">

                <div class="row">
                    <div class="col border" style="background-color:lightgreen;">

                        Nuotraukos Įkėlimas


                    </div>
                </div>

            </div>
            <div class="col-9" style="background-color:lavenderblush;">

                <select name="blog_illustration" class = "form-control">
                    <option value="adresas1">Nuotrauka1</option>
                    <option value="adresas2">Nuotrauka2</option>
                    <option value="adresas3">Nuotrauka3</option>
                </select>
            </div>

        </div>
        <button type="submit" class="btn btn-primary border" name="progress" value="1">Pildyti (sub)kategorijas</button>



    </form>

    <form action="pagrindinis_puslapis">
        <div class="control">
            <button type="submit" class="btn btn-primary border">Pridėti Kategoriją</button>
        </div>

    </form>






    @include('modal')
    @endsection
