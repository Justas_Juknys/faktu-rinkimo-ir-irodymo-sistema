@extends('layout')
@section('title')
    Kontaktai
    @endsection
@section('javascript')
    <script type="text/javascript">

        function addbold() {
            $(comment).surroundSelectedText("<b>", "</b>");
        }

        function additalic() {
            $(comment).surroundSelectedText("<i>", "</i>");
        }

        function addunderline() {
            $(comment).surroundSelectedText("<ins>", "</ins>");
        }

        function addlower() {
            $(comment).surroundSelectedText("<sub>", "</sub>");
        }

        function addupper() {
            $(comment).surroundSelectedText("<sup>", "</sup>");
        }

        function addhyperlink() {
            $(comment).surroundSelectedText("<a href=\"www.jusuadresas.com\">", "</a>");
        }

        function Cerase()
        {
            document.getElementById('comment').value = "";
        }

        function addemoticon(E)
        {
            var e = document.getElementById(E);
            $(comment).surroundSelectedText("", e.options[e.selectedIndex].text);
        }

        function addcolor(E)
        {
            var e = document.getElementById(E).value;
            var color = "<font color=" + e + ">";
            $(comment).surroundSelectedText(color,"</font>");
        }

        function addfont(E)
        {
            var e = document.getElementById(E).value;
            if (e==1) $(comment).surroundSelectedText("<nobr class=a>","</nobr>");
            else if (e==2) $(comment).surroundSelectedText("<nobr class=c>","</nobr>");
        }
    </script>
    @endsection
@section('main')
    <div class="row">

        <div class="col border" style="background-color:lightgreen;">

            Susiekti su


        </div>
        <div class="col" style="background-color:lavenderblush;">

            <button type="button" class="btn btn-primary btn-block border">Moderatoriais (Pranešti Pažeidimą)</button>


        </div>

        <div class="col" style="background-color:lavenderblush;">

            <button type="button" class="btn btn-primary btn-block border">Moderatoriais (Patvirtinti Specialų Vartotoją)</button>

        </div>

        <div class="col" style="background-color:lavenderblush;">

            <button type="button" class="btn btn-primary btn-block border">Administratoriumi(Paslaugų Pasiūlymui)</button>

        </div>


    </div>

    <div class="row">

        <div class="col" style="background-color:lavenderblush;">

            <button type="button" class="btn btn-primary btn-block border">Rašyti Naują Pranešimą</button>

        </div>

        <div class="col" style="background-color:lavenderblush;">

            <button type="button" class="btn btn-primary btn-block border">Cituoti</button>

        </div>


    </div>

    <div class="row">

        <div class="col-2 border" style="background-color:lightgreen;">

            Pokalbio Numeris


        </div>
        <div class="col-8 border" style="background-color:lightgreen;">

            Pranešimo Tekstas


        </div>
        <div class="col-2 border" style="background-color:lightgreen;">

            Pranešimo Autorius


        </div>
    </div>

    <div class="row">
        <div class="col-2 border">
            <div class="row">
                <div class="col border">
                    <div class="row">
                        <div class="col border" style="background-color:lightblue;height:100px;">

                            <p class="jautru">Pokalbio Numeris</p>


                        </div>
                    </div>
                    <div class="row">
                        <div class="col border" style="background-color:lightblue;height:100px;">

                            <p class="jautru">Pranešimo Data</p>


                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-8 border" style="background-color:lightyellow;">

            Pranešimas1


        </div>

        <div class="col-2 border" style="background-color:lightblue;">

            Pranešimao Siuntėjas


        </div>

    </div>

    <div class="row">
        <div class="col-2 border">
            <div class="row">
                <div class="col border">
                    <div class="row">
                        <div class="col border" style="background-color:lightblue;height:100px;">

                            <p class="jautru">Pokalbio Numeris</p>


                        </div>
                    </div>
                    <div class="row">
                        <div class="col border" style="background-color:lightblue;height:100px;">

                            <p class="jautru">Pranešimo Data</p>


                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-8 border" style="background-color:lightyellow;">

            Pranešimas2


        </div>

        <div class="col-2 border" style="background-color:lightblue;">

            Pranešimao Siuntėjas


        </div>

    </div>

    <div class="row">
        <div class="col-2 border">
            <div class="row">
                <div class="col border">
                    <div class="row">
                        <div class="col border" style="background-color:lightblue;height:100px;">

                            <p class="jautru">Pokalbio Numeris</p>


                        </div>
                    </div>
                    <div class="row">
                        <div class="col border" style="background-color:lightblue;height:100px;">

                            <p class="jautru">Pranešimo Data</p>


                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-8 border" style="background-color:lightyellow;">

            Pranešimas3


        </div>

        <div class="col-2 border" style="background-color:lightblue;">

            Pranešimao Siuntėjas


        </div>

    </div>

    <div class="row">
        <div class="col-2 border">
            <div class="row">
                <div class="col border">
                    <div class="row">
                        <div class="col border" style="background-color:lightblue;height:100px;">

                            <p class="jautru">Pokalbio Numeris</p>


                        </div>
                    </div>
                    <div class="row">
                        <div class="col border" style="background-color:lightblue;height:100px;">

                            <p class="jautru">Pranešimo Data</p>


                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-8 border" style="background-color:lightyellow;">

            Pranešimas4


        </div>

        <div class="col-2 border" style="background-color:lightblue;">

            Pranešimao Siuntėjas


        </div>

    </div>


        <div class="row">
            <div class="col-sm-2">

                <img class="card-img-fluid" src="{{ asset('img/Vartotojas4.png') }}" alt="Vartotojas" style="max-width:100%">
                <div class="btn-group-vertical btn-block">
                    <button type="button" class="btn btn-primary border">Spausdinti Komentarą</button>
                    <button type="button" class="btn btn-primary border">Kopijuoti Komentarą</button>
                    <button type="button" class="btn btn-primary border" onclick="Cerase();">Išvalyti Komentarą</button>
                </div>


            </div>
            <div class="col-sm-10">

                <div class="d-flex flex-wrap">
                    <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addunderline();">Pabraukimas</button> </div>
                    <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addbold();" >Paryškinimas</button> </div>
                    <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="additalic()">Pasvyrimas</button> </div>
                    <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addlower();">Apatinis Ind.</button> </div>
                    <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addupper();"> Viršutinis Ind.</button> </div>
                    <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border">Parinktis</button> </div>

                    <div class="p-0 bg-warning">

                        <select class="form-control" id="fonts" name="sriftas" onchange="addfont('fonts');">
                            <option value=1>Times New Roman</option>
                            <option value=2>Comic Sans MS</option>
                        </select>

                    </div>
                    <div class="p-0 bg-warning "> <input type="color" id="Icolor" value="#ff0000" size="35" onchange="addcolor('Icolor');">   </div>
                    <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addhyperlink();">Hipersaitai</button> </div>
                    <div class="p-0 bg-warning "><button type="button" id="Mpreview1" class="btn btn-primary border">Įkelti failą</button> </div>
                    <div class="p-0 bg-warning ">

                        <select class="form-control" id="emoticons" name="sypseneles" onchange="addemoticon('emoticons')";>
                            <option>:D</option>
                            <option>:P</option>
                            <option>:)</option>
                        </select>

                    </div>
                    <div class="p-0 bg-warning"><button type="button" class="btn btn-primary border" onclick="preview();" data-toggle="modal" data-target="#myModal">Pirminė Vers.</button> </div>
                </div>

                <div class="form-group">
                    <textarea class="form-control" rows="8" id="comment"></textarea>
                </div>

            </div>
        </div>
    @endsection
@extends('modal')
