<div class="row">
    <div class="col-sm-2">

        <img class="card-img-fluid" src="{{ asset('img/Vartotojas4.png') }}" alt="Vartotojas" style="max-width:100%">
        <div class="btn-group-vertical btn-block">
            <button type="submit" class="btn btn-primary border">Talpinti Įrašą</button>
            <button type="button" class="btn btn-primary border">Kopijuoti Įrašą</button>
            <button type="button" class="btn btn-primary border" onclick="Cerase();">Išvalyti Įrašą</button>
        </div>


    </div>

<div class="col-sm-10">

    <div class="d-flex flex-wrap">
        <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addunderline();">Pabraukimas</button> </div>
        <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addbold();" >Paryškinimas</button> </div>
        <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="additalic()">Pasvyrimas</button> </div>
        <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addlower();">Apatinis Ind.</button> </div>
        <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addupper();"> Viršutinis Ind.</button> </div>
        <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border">Parinktis</button> </div>

        <div class="p-0 bg-warning">

            <select class="form-control" id="fonts" name="sriftas" onchange="addfont('fonts');">
                <option value=1>Times New Roman</option>
                <option value=2>Comic Sans MS</option>
            </select>

        </div>
        <div class="p-0 bg-warning "> <input type="color" id="Icolor" value="#ff0000" size="35" onchange="addcolor('Icolor');">   </div>
        <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addhyperlink();">Hipersaitai</button> </div>
        <div class="p-0 bg-warning "><button type="button" id="Mpreview1" class="btn btn-primary border">Įkelti failą</button> </div>
        <div class="p-0 bg-warning ">

            <select class="form-control" id="emoticons" name="sypseneles" onchange="addemoticon('emoticons')";>
                <option>:D</option>
                <option>:P</option>
                <option>:)</option>
            </select>

        </div>
        <div class="p-0 bg-warning"><button type="button" class="btn btn-primary border" onclick="preview();" data-toggle="modal" data-target="#myModal">Pirminė Vers.</button> </div>
    </div>

    <div class="form-group">
        <textarea class="form-control" rows="8" id="comment" name="blog_text"></textarea>
    </div>

</div>
</div>
