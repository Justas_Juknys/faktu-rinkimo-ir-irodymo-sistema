@extends('layout')
@section('main')
    @foreach($categories as $category)
        <div class="row">

            <div class="col-3" style="background-color:lavenderblush;">

                <div class="row">
                    <div class="col border" style="background-color:lightgreen;">

                        <a href="kategorijos/{{$category->category_id}}"> {{$category->category_title}}</a>


                    </div>
                </div>

            </div>
            <div class="col-9" style="background-color:lavenderblush;">

                <div class="row">
                    <div class="col border" style="background-color:LemonChiffon;">

                        {{$category->category_description}}


                    </div>
                </div>

            </div>

        </div>
        @endforeach
<form action="prid_ti_kategorij_">
    <div class="control">
        <button type="submit" class="btn btn-primary border">Pridėti Kategoriją</button>
    </div>

</form>
    @endsection
