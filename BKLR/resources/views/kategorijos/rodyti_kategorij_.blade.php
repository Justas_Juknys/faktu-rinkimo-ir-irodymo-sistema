@extends('layout')
@section('main')
    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    {{$rez->category_title}}


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:LemonChiffon;">

                    {{$rez->category_description}}


                </div>
            </div>

        </div>

    </div>

    <div class="row">

        <div class="col" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightblue;">

                    Subkategorijos


                </div>
            </div>

        </div>

    </div>
    @foreach($rez->subcategories as $subcategory)
        <div class="row">

            <div class="col-3" style="background-color:lavenderblush;">

                <div class="row">
                    <div class="col border" style="background-color:lightgreen;">

                        {{$subcategory->subcategory_title}}


                    </div>
                </div>

            </div>
            <div class="col-9" style="background-color:lavenderblush;">

                <div class="row">
                    <div class="col border" style="background-color:LemonChiffon;">

                        {{$subcategory->subcategory_description}}


                    </div>
                </div>

            </div>

        </div>
        @endforeach
    <form method="GET" action="/BKLR/public/prid_ti_subkategorij_">
        @csrf
        <input type="hidden" value="{{$rez->category_id}}" name="category_id">
        <div class="control">
            <button type="submit" class="btn btn-primary border">Pridėti subkategoriją</button>
        </div>

    </form>
    @endsection
