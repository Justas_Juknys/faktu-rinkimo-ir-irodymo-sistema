@extends('layout')
@section('main')
    <form method="POST" action="/BKLR/public/papildoma_subkategorija">
        @csrf

        <input type="hidden" name="blog_id" value="{{$blog_id}}">

        <div class="row">

            <div class="col-3" style="background-color:lavenderblush;">

                <div class="row">
                    <div class="col border" style="background-color:lightgreen;">

                        Papildoma Kategorija


                    </div>
                </div>

            </div>
            <div class="col-9" style="background-color:lavenderblush;">

                <div class="row">
                    <select name="category_id" class = "form-control">
                        @foreach ($categories as $category)
                            @if(array_search($category->category_id,$category_list)===false)
                        <option value="{{$category->category_id}}">{{$category->category_title}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

            </div>

        </div>



    <div class="control">
        <button type="submit" class="btn btn-primary border">Patvirtinti Pasirinkimą</button>
    </div>

</form>
    @endsection
