@extends('layout')
@section('title')
    Nemokamos Paslaugos
    @endsection
@section('javascript')
    <script type="text/javascript">
        function rate()
        {
            var a = $('input[name=optradio]:checked').val();
            var fieldNameElement = document.getElementById('Rplace').innerText;
            document.getElementById('Rplace').innerHTML = a;
        }

        function Rerase()
        {
            $('input[name=optradio]').prop('checked',false);
            document.getElementById('Rplace').innerHTML = "Įvertinimas";
        }

        function addbold() {
            $(comment).surroundSelectedText("<b>", "</b>");
        }

        function additalic() {
            $(comment).surroundSelectedText("<i>", "</i>");
        }

        function addunderline() {
            $(comment).surroundSelectedText("<ins>", "</ins>");
        }

        function addlower() {
            $(comment).surroundSelectedText("<sub>", "</sub>");
        }

        function addupper() {
            $(comment).surroundSelectedText("<sup>", "</sup>");
        }

        function addhyperlink() {
            $(comment).surroundSelectedText("<a href=\"www.jusuadresas.com\">", "</a>");
        }

        function Cerase()
        {
            document.getElementById('comment').value = "";
        }

        function addemoticon(E)
        {
            var e = document.getElementById(E);
            $(comment).surroundSelectedText("", e.options[e.selectedIndex].text);
        }

        function addcolor(E)
        {
            var e = document.getElementById(E).value;
            var color = "<font color=" + e + ">";
            $(comment).surroundSelectedText(color,"</font>");
        }

        function addfont(E)
        {
            var e = document.getElementById(E).value;
            if (e==1) $(comment).surroundSelectedText("<nobr class=a>","</nobr>");
            else if (e==2) $(comment).surroundSelectedText("<nobr class=c>","</nobr>");
        }
    </script>
    @endsection
@section('main')
    <div class="container-fluid">
        <h1>Siūlomos Paslaugos</h1>
    </div>


    <div class="table-responsive">
        <table class=table>
            <thead>
            <tr>
                <th>Paslaugos Pavadinimas</th>
                <th>Paslaugos Pasirinkimai</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Naujienlaiškis</td>
                <td>  <select class="form-control" id="Option1" name="pasirinkimas1">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td> <button type="button" class="btn btn-primary border">Užsisakyti</button></td>
            </tr>
            <tr>
                <td>Tekstinė Medžiaga</td>
                <td>  <select class="form-control" id="Option2" name="pasirinkimas2">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td> <button type="button" class="btn btn-primary border">Užsisakyti</button></td>
            </tr>
            <tr>
                <td>Video Medžiaga</td>
                <td>  <select class="form-control" id="Option3" name="pasirinkimas3">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td> <button type="button" class="btn btn-primary border">Užsisakyti</button></td>
            </tr>
            <tr>
                <td>Citatos Įkėlimas</td>
                <td>  <select class="form-control" id="Option4" name="pasirinkimas4">
                        <option>Pasirinkimas1</option>
                        <option>Pasirinkimas2</option>
                        <option>Pasirinkimas3</option>
                    </select></td>
                <td> <button type="button" class="btn btn-primary border">Užsisakyti</button></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="form-group">
        <textarea class="form-control" rows="5" id="comment" style="max-width:100%;">Citatos Įvedimas</textarea>
    </div>

    <div class="container-fluid">
        <h1>Plačiau Apie Paslaugą</h1>
    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Paslauga


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <select class="form-control" id="Servise" name="Paslauga">
                <option>Paslauga1</option>
                <option>Paslauga2</option>
                <option>Paslauga3</option>
            </select>

        </div>

    </div>

    <div class="row">

        <div class="col-6" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightyellow;">

                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisi risus, varius at est non, suscipit ultricies odio. Ut volutpat condimentum ipsum, id sollicitudin felis congue at. Aliquam erat volutpat. Praesent vitae est quis metus facilisis tempor sed cursus nibh. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi vitae ullamcorper mauris. Nam sit amet sodales erat. Duis congue enim vel nibh hendrerit, eget porttitor odio finibus. Curabitur vehicula tellus ut nisi venenatis, in feugiat urna condimentum. Morbi in velit rhoncus, convallis felis eu, dignissim nulla. Sed porta sit amet velit consequat congue.


                </div>
            </div>

        </div>
        <div class="col-6" style="background-color:lavenderblush;">

            <img class="img-fluid" src="{{ asset('img/Paslauga1.png') }}" alt="Paslauga">

        </div>

    </div>

    <div class="row">

        <div class="col" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen">

                    Pirkta


                </div>
            </div>

        </div>
        <div class="col" style="background-color:lightyellow;">

            X

        </div>

        <div class="col border" style="background-color:lightgreen">

            Pardutoa


        </div>

        <div class="col" style="background-color:lightyellow;">

            Y

        </div>

    </div>

    <div class="d-flex">
        <div id="Rplace" class="p-2 bg-info flex-fill border">Vidutinis Įvertinimas</div>

        <div class="card-group flex-fill">
            <div class="card bg-primary">

                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=0 class="form-check-input" name="optradio">0/5</input>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=1 class="form-check-input" name="optradio">1/5</input>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=2 class="form-check-input" name="optradio">2/5</input>
                    </label>
                </div>

            </div>
            <div class="card bg-primary">

                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=3 class="form-check-input" name="optradio">3/5</input>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=4 class="form-check-input" name="optradio">4/5</input>
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input type="radio" value=5 class="form-check-input" name="optradio">5/5</input>
                    </label>
                </div>

            </div>
        </div>



        <button type="button" class="btn btn-primary border flex-fill" onclick="rate()">Įvertinti</button>
        <button type="button" class="btn btn-primary border flex-fill" onclick="Rerase()">Atšaukti Įvertinimą</button>
    </div>

    <div class="btn-group btn-block" style="max-height:100px">
        <button type="button" class="btn btn-primary border">Rodyti Komentarus</button>
        <button type="button" class="btn btn-primary border">Rašyti Komentarus</button>
    </div>

    <div class="container-fluid">
        <h1>Paslaugos Komentarai</h1>
    </div>
    <div class="row">
        <div class="col-sm-10">
            <div class="media border">
                <img src="{{ asset('img/Vartotojas1.png') }}" alt="Vartotojas" class="mr-3 mt-3 rounded-circle" style="width:60px;">
                <div class="media-body">
                    <h4>Vartotojas <small><i>Komentaro Pavadinimas</i></small></h4>
                    <p>Komentaro tekstas</p>
                </div>
            </div>
        </div>
        <div class="col-sm-2">

            <div class="btn-group-vertical btn-block">
                <button type="button" class="btn btn-primary border">Cituoti</button>
                <button type="button" class="btn btn-primary border">Įvertinti</button>
                <button type="button" class="btn btn-primary border">Pranešti apie pažeidimą</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10">
            <div class="media border">
                <img src="{{ asset('img/Vartotojas2.png') }}" alt="Vartotojas" class="mr-3 mt-3 rounded-circle" style="width:60px;">
                <div class="media-body">
                    <h4>Vartotojas <small><i>Komentaro Pavadinimas</i></small></h4>
                    <p>Komentaro tekstas</p>
                </div>
            </div>
        </div>
        <div class="col-sm-2">

            <div class="btn-group-vertical btn-block">
                <button type="button" class="btn btn-primary border">Cituoti</button>
                <button type="button" class="btn btn-primary border">Įvertinti</button>
                <button type="button" class="btn btn-primary border">Pranešti apie pažeidimą</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10">
            <div class="media border">
                <img src="{{ asset('img/Vartotojas3.png') }}" alt="Vartotojas" class="mr-3 mt-3 rounded-circle" style="width:60px;">
                <div class="media-body">
                    <h4>Vartotojas <small><i>Komentaro Pavadinimas</i></small></h4>
                    <p>Komentaro tekstas</p>
                </div>
            </div>
        </div>
        <div class="col-sm-2">

            <div class="btn-group-vertical btn-block">
                <button type="button" class="btn btn-primary border">Cituoti</button>
                <button type="button" class="btn btn-primary border">Įvertinti</button>
                <button type="button" class="btn btn-primary border">Pranešti apie pažeidimą</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-2">

            <img class="card-img-fluid" src="{{ asset('img/Vartotojas4.png') }}" alt="Vartotojas" style="max-width:100%">
            <div class="btn-group-vertical btn-block">
                <button type="button" class="btn btn-primary border">Spausdinti Komentarą</button>
                <button type="button" class="btn btn-primary border">Kopijuoti Komentarą</button>
                <button type="button" class="btn btn-primary border" onclick="Cerase();">Išvalyti Komentarą</button>
            </div>


        </div>
        <div class="col-sm-10">

            <div class="d-flex flex-wrap">
                <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addunderline();">Pabraukimas</button> </div>
                <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addbold();" >Paryškinimas</button> </div>
                <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="additalic()">Pasvyrimas</button> </div>
                <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addlower();">Apatinis Ind.</button> </div>
                <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addupper();"> Viršutinis Ind.</button> </div>
                <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border">Parinktis</button> </div>

                <div class="p-0 bg-warning">

                    <select class="form-control" id="fonts" name="sriftas" onchange="addfont('fonts');">
                        <option value=1>Times New Roman</option>
                        <option value=2>Comic Sans MS</option>
                    </select>

                </div>
                <div class="p-0 bg-warning "> <input type="color" id="Icolor" value="#ff0000" size="35" onchange="addcolor('Icolor');">   </div>
                <div class="p-0 bg-warning "><button type="button" class="btn btn-primary border" onclick="addhyperlink();">Hipersaitai</button> </div>
                <div class="p-0 bg-warning "><button type="button" id="Mpreview1" class="btn btn-primary border">Įkelti failą</button> </div>
                <div class="p-0 bg-warning ">

                    <select class="form-control" id="emoticons" name="sypseneles" onchange="addemoticon('emoticons')";>
                        <option>:D</option>
                        <option>:P</option>
                        <option>:)</option>
                    </select>

                </div>
                <div class="p-0 bg-warning"><button type="button" class="btn btn-primary border" onclick="preview();" data-toggle="modal" data-target="#myModal">Pirminė Vers.</button> </div>
            </div>

            <div class="form-group">
                <textarea class="form-control" rows="8" id="comment"></textarea>
            </div>

        </div>
    </div>
    <div class="container-fluid">
        <h1>Jau Užsakytos Paslaugos</h1>
    </div>

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Paslaugos Numeris</th>
                <th>Paslaugos Pavadinimas</th>
                <th>Paslaugos Pasirinkimai</th>
                <th>Užsisakymo Data</th>
                <th>Paslaugos Statusas</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>#1</td>
                <td>Paslauga1</td>
                <td>Pasirinkimai1</td>
                <td>Data1</td>
                <td>Statusas1</td>
                <td><button type="button" class="btn btn-primary border">Atšaukti</button></td>
            </tr>
            <tr>
                <td>#2</td>
                <td>Paslauga2</td>
                <td>Pasirinkimai2</td>
                <td>Data2</td>
                <td>Statusas2</td>
                <td><button type="button" class="btn btn-primary border">Atšaukti</button></td>
            </tr>
            <tr>
                <td>#3</td>
                <td>Paslauga3</td>
                <td>Pasirinkimai3</td>
                <td>Data3</td>
                <td>Statusas3</td>
                <td><button type="button" class="btn btn-primary border">Atšaukti</button></td>
            </tr>
            <tr>
                <td>#4</td>
                <td>Paslauga4</td>
                <td>Pasirinkimai4</td>
                <td>Data4</td>
                <td>Statusas4</td>
                <td><button type="button" class="btn btn-primary border">Atšaukti</button></td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection
@extends('modal')
