@extends('layout')
@section('title')
    Vartotojo Sąsaja
    @endsection
@section('main')

    <div class="row">
        <div class="col border" style="background-color:lightgreen;">

            Vartotojo Informacija


        </div>
    </div>



    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Vartotojo Slapyvardis


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lavender;">

                    {{$user->user_nickname}}


                </div>
            </div>

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Vartotojo Gimimo Diena


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lavender;">

                    {{$user->user_birth_date}}


                </div>
            </div>

        </div>

    </div>



    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Vartotojo E-Paštas


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">


            <div class="row">
                <div class="col border" style="background-color:lavender;">

                    {{$user->user_email}}


                </div>
            </div>


        </div>


    </div>



    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Vartotojo Vardas


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">


            <div class="row">
                <div class="col border" style="background-color:lavender;">

                    {{$user->user_name}}


                </div>
            </div>


        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Vartotojo Pavardė


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lavender;">

                    {{$user->user_surname}}


                </div>
            </div>

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Vartotojo Adresas


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lavender;">

                    {{$user->user_address}}


                </div>
            </div>

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Vartotojo Telefonas


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lavender;">

                    {{$user->user_telephone}}


                </div>
            </div>

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Atsiskaitymo Būdas


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lavender;">

                    @if($user->user_payment_method==0)
                        Kortelė
                        @else
                    Bankinis pavedimas
                        @endif


                </div>
            </div>

        </div>

    </div>





    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Lytis


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lavender;">
                @if($user->user_sex==0)
                    Vyras
                @else
                    Moteris
                @endif


                </div>
            </div>

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Politinis Judėjimas


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lavender;">

                    {{$user->user_political_alignment}}


                </div>
            </div>

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Religija/Tikėjimas


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lavender;">

                    {{$user->user_spiritual_beliefs}}


                </div>
            </div>

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Profesija


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lavender;">

                    {{$user->user_profession}}


                </div>
            </div>

        </div>

    </div>
    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Papildoma Informacija (<255 Simb.)


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lavender;min-height: 50px">

                    {{$user->user_extra_information}}


                </div>
            </div>

        </div>

    </div>
    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    1 Hipersaito Nuoroda


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lavender;">

                    {{$user->user_hyperlink}}


                </div>
            </div>

        </div>

    </div>


    <button type="submit" class="btn btn-primary btn-block border">Redaguoti Nustatymus</button>




    @endsection
