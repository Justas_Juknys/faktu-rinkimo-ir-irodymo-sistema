@extends('layout')
@section('title')
    Registracija
    @endsection
@section('main')

    <div class="row">
        <div class="col border" style="background-color:lightgreen;">

            Būtina Informacija


        </div>
    </div>
        <form method="POST" action="pagrindinis_puslapis/reg" style="margin-button: 1em;">
            {{csrf_field()}}


    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Vartotojo Slapyvardis


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <input type="text" class = "form-control" name = "user_nickname" >

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Vartotojo Gimimo Diena


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <input type="date" class = "form-control" name = "user_birth_date" >

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Vartotojo Slaptažodis

                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <input type="text" class = "form-control" name = "user_password" >

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Pakartotas Slaptažodis


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <input type="text" class = "form-control" name = "user_password_repeated" >

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Vartotojo Elektroninis Paštas


                </div>
            </div>

        </div>
        <div class="col-6" style="background-color:lavenderblush;">


                <input type="text" class = "form-control" name = "user_email" >


        </div>

        <div class="col-3" style="background-color:lavenderblush;">
            <button type="button" class="btn btn-primary border">Gauti Patvirtinimo Kodą</button>
        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Patvirtinimo Kodas


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">


                <input type="text" class = "form-control" name = "user_confirmation_code" >


        </div>

    </div>

    <div class="row">
        <div class="col border" style="background-color:lightgreen;">

            Pirkimams Būtina Informacija


        </div>
    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Vartotojo Vardas


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">


                <input type="text" class = "form-control" name = "user_name" >


        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Vartotojo Pavardė


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <input type="text" class = "form-control" name = "user_surname" >

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Vartotojo Adresas


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <input type="text" class = "form-control" name = "user_address" >

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Vartotojo Telefonas


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <input type="text" class = "form-control" name = "user_telephone" >

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Atsiskaitymo Būdas


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <select name="user_payment_method" class = "form-control">
                <option value="0">kortelė</option>
                <option value="1">bankinis pavedimas</option>
            </select>
        </div>

    </div>














    <div class="row">
        <div class="col border" style="background-color:lightgreen;">

            Nebūtina Informacija


        </div>
    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Lytis


                </div>
            </div>

        </div>
        <div class="col-9"  style="background-color:lavenderblush;">

            <select name="user_sex" class = "form-control">
                <option value="0">Vyras</option>
                <option value="1">Moteris</option>
            </select>

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Politinis Judėjimas


                </div>
            </div>


        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <input type="text" class = "form-control" name = "user_political_alignment" >

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Religija/Tikėjimas


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <input type="text" class = "form-control" name = "user_spiritual_beliefs" >

        </div>

    </div>

    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Profesija


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <input type="text" class = "form-control" name = "user_profession" >

        </div>

    </div>
    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    Papildoma Informacija (<255 Simb.)


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <input type="text" class = "form-control" name = "user_extra_information" >

        </div>

    </div>
    <div class="row">

        <div class="col-3" style="background-color:lavenderblush;">

            <div class="row">
                <div class="col border" style="background-color:lightgreen;">

                    1 Hipersaito Nuoroda


                </div>
            </div>

        </div>
        <div class="col-9" style="background-color:lavenderblush;">

            <input type="text" class = "form-control" name = "user_hyperlink" >

        </div>

    </div>


    <button type="submit" class="btn btn-primary btn-block border">Registruotis</button>
    </form>


    <div class="row">
        <div class="col border" style="background-color:yellow;">

            Pranešimas Nesėkmingos Registracijos Atveju


        </div>
    </div>
    @endsection
