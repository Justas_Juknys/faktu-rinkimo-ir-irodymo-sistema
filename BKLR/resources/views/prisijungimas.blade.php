@extends('layout')
@section('title')
    Prisijungimas
    @endsection
@section('main')
    <form method="GET" action="prisijungimas/duom" style="margin-button: 1em;">
        <div class="row">

            <div class="col-3" style="background-color:lavenderblush;">

                <div class="row">
                    <div class="col border" style="background-color:lightgreen;">

                        Vartotojo Slapyvardis


                    </div>
                </div>

            </div>
            <div class="col-9" style="background-color:lavenderblush;">

                <input type="text" class = "form-control" name = "user_nickname" >

            </div>

        </div>

        <div class="row">

            <div class="col-3" style="background-color:lavenderblush;">

                <div class="row">
                    <div class="col border" style="background-color:lightgreen;">

                        Vartotojo Slaptažodis

                    </div>
                </div>

            </div>
            <div class="col-9" style="background-color:lavenderblush;">

                <input type="password" class = "form-control" name = "user_password" >

            </div>

        </div>

        <button type="submit" class="btn btn-primary btn-block border">Prisijungti</button>

    </form>
    @endsection
