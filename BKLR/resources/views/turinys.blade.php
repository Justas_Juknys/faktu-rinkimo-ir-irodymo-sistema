@extends('layout')
@section('title')
    Turinys
    @endsection
@section('main')
    <div class="d-flex flex-wrap">
        @foreach($categories as $category)
        <div class="btn-group">
            <button type="button" class="btn btn-primary border">{{$category->category_title}}</button>
            <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split border" data-toggle="dropdown">
            </button>
            <div class="dropdown-menu">
                @foreach($category->subcategories as $subcategory)
                <a class="dropdown-item" href="#">{{$subcategory->subcategory_title}}</a>
                    @endforeach
            </div>
        </div>
        @endforeach
    </div>

    <div class="d-flex">
        <div class="p-2 bg-info flex-fill">Pageidaujamas Laikotarpis</div>
    </div>

    <div class="row">
        <div class="col-6" style="background-color:DarkTurquoise;">
            <div class="d-flex">
                <div class="pt-0" style="max-width:100%;background-color:DarkTurquoise;">Nuo</div>
            </div>
            <select class="form-control" id="Fyear" name="NuoMetu">
                <option>2019</option>
                <option>2020</option>
            </select>
            <select class="form-control" id="Fmonth" name="NuoMenesio">
                <option>Sausis</option>
                <option>Vasaris</option>
                <option>Kovas</option>
                <option>Balandis</option>
                <option>Gegužė</option>
                <option>Birželis</option>
                <option>Liepa</option>
                <option>Rugpjūtis</option>
                <option>Rugsėjis</option>
                <option>Spalis</option>
                <option>Lapkritis</option>
                <option>Gruodis</option>
            </select>
        </div>
        <div class="col-6" style="background-color:DarkTurquoise;">
            <div class="d-flex">
                <div class="pt-0" style="max-width:100%;background-color:DarkTurquoise;">Iki</div>
            </div>
            <select class="form-control" id="Tyear" name="IkiMetu">
                <option>2019</option>
                <option>2020</option>
            </select>
            <select class="form-control" id="Tmonth" name="IkiMenesio">
                <option>Sausis</option>
                <option>Vasaris</option>
                <option>Kovas</option>
                <option>Balandis</option>
                <option>Gegužė</option>
                <option>Birželis</option>
                <option>Liepa</option>
                <option>Rugpjūtis</option>
                <option>Rugsėjis</option>
                <option>Spalis</option>
                <option>Lapkritis</option>
                <option>Gruodis</option>
            </select>
        </div>
    </div>



    <div class="row">
        <div class="col-7 border" style="background-color:Turquoise ;">

            Įrašas #1

        </div>
        <div class="col-5 border" style="background-color:Turquoise;">

            kategorijos/subkategorijos

        </div>

    </div>

    <div class="row">
        <div class="col-7 border" style="background-color:LightYellow;">

            TRUMPA SANTRAUKA Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisi risus, varius at est non, suscipit ultricies odio. Ut volutpat condimentum ipsum, id sollicitudin felis congue at. Aliquam erat volutpat. Praesent vitae est quis metus facilisis tempor sed cursus nibh. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi vitae ullamcorper mauris. Nam sit amet sodales erat. Duis congue enim vel nibh hendrerit, eget porttitor odio finibus. Curabitur vehicula tellus ut nisi venenatis, in feugiat urna condimentum. Morbi in velit rhoncus, convallis felis eu, dignissim nulla. Sed porta sit amet velit consequat congue. Quisque accumsan nibh ultricies turpis placerat, a maximus purus interdum. Mauris justo lorem, iaculis sit amet pharetra vel, accumsan eu sapien. Quisque viverra purus ac elit condimentum egestas.

        </div>
        <div class="col-3 border" style="background-color:LightYellow ;">

            <img class="card-img-fluid" src="{{ asset('img/nuotrauka1.png') }}" alt="Nuotrauka" style="max-width:100%">

        </div>
        <div class="col-2 border" style="background-color:Turquoise;">

            <button type="button" class="btn btn-primary border">Eiti į Įrašą</button>
            <button type="button" class="btn btn-primary border">Eiti į Komentarus</button>
            <button type="button" class="btn btn-primary border">Įrašo Data</button>
            <button type="button" class="btn btn-primary border">Įrašo Autorius</button>

        </div>

    </div>


    <div class="row">
        <div class="col-7 border" style="background-color:Turquoise ;">

            Įrašas #2

        </div>
        <div class="col-5 border" style="background-color:Turquoise;">

            kategorijos/subkategorijos

        </div>

    </div>

    <div class="row">
        <div class="col-7 border" style="background-color:LightYellow;">

            TRUMPA SANTRAUKA Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisi risus, varius at est non, suscipit ultricies odio. Ut volutpat condimentum ipsum, id sollicitudin felis congue at. Aliquam erat volutpat. Praesent vitae est quis metus facilisis tempor sed cursus nibh. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi vitae ullamcorper mauris. Nam sit amet sodales erat. Duis congue enim vel nibh hendrerit, eget porttitor odio finibus. Curabitur vehicula tellus ut nisi venenatis, in feugiat urna condimentum. Morbi in velit rhoncus, convallis felis eu, dignissim nulla. Sed porta sit amet velit consequat congue. Quisque accumsan nibh ultricies turpis placerat, a maximus purus interdum. Mauris justo lorem, iaculis sit amet pharetra vel, accumsan eu sapien. Quisque viverra purus ac elit condimentum egestas.

        </div>
        <div class="col-3 border" style="background-color:LightYellow ;">

            <img class="card-img-fluid" src="{{ asset('img/nuotrauka2.png') }}" alt="Nuotrauka" style="max-width:100%">

        </div>
        <div class="col-2 border" style="background-color:Turquoise;">

            <button type="button" class="btn btn-primary border">Eiti į Įrašą</button>
            <button type="button" class="btn btn-primary border">Eiti į Komentarus</button>
            <button type="button" class="btn btn-primary border">Įrašo Data</button>
            <button type="button" class="btn btn-primary border">Įrašo Autorius</button>

        </div>

    </div>




    <div class="row">
        <div class="col-7 border" style="background-color:Turquoise ;">

            Įrašas #3

        </div>
        <div class="col-5 border" style="background-color:Turquoise;">

            kategorijos/subkategorijos

        </div>

    </div>

    <div class="row">
        <div class="col-7 border" style="background-color:LightYellow;">

            TRUMPA SANTRAUKA Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisi risus, varius at est non, suscipit ultricies odio. Ut volutpat condimentum ipsum, id sollicitudin felis congue at. Aliquam erat volutpat. Praesent vitae est quis metus facilisis tempor sed cursus nibh. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi vitae ullamcorper mauris. Nam sit amet sodales erat. Duis congue enim vel nibh hendrerit, eget porttitor odio finibus. Curabitur vehicula tellus ut nisi venenatis, in feugiat urna condimentum. Morbi in velit rhoncus, convallis felis eu, dignissim nulla. Sed porta sit amet velit consequat congue. Quisque accumsan nibh ultricies turpis placerat, a maximus purus interdum. Mauris justo lorem, iaculis sit amet pharetra vel, accumsan eu sapien. Quisque viverra purus ac elit condimentum egestas.

        </div>
        <div class="col-3 border" style="background-color:LightYellow ;">

            <img class="card-img-fluid" src="{{ asset('img/nuotrauka3.png') }}" alt="Nuotrauka" style="max-width:100%">

        </div>
        <div class="col-2 border" style="background-color:Turquoise;">

            <button type="button" class="btn btn-primary border">Eiti į Įrašą</button>
            <button type="button" class="btn btn-primary border">Eiti į Komentarus</button>
            <button type="button" class="btn btn-primary border">Įrašo Data</button>
            <button type="button" class="btn btn-primary border">Įrašo Autorius</button>

        </div>

    </div>





    <div class="row">
        <div class="col-7 border" style="background-color:Turquoise ;">

            Įrašas #4

        </div>
        <div class="col-5 border" style="background-color:Turquoise;">

            kategorijos/subkategorijos

        </div>

    </div>

    <div class="row">
        <div class="col-7 border" style="background-color:LightYellow;">

            TRUMPA SANTRAUKA Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nisi risus, varius at est non, suscipit ultricies odio. Ut volutpat condimentum ipsum, id sollicitudin felis congue at. Aliquam erat volutpat. Praesent vitae est quis metus facilisis tempor sed cursus nibh. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi vitae ullamcorper mauris. Nam sit amet sodales erat. Duis congue enim vel nibh hendrerit, eget porttitor odio finibus. Curabitur vehicula tellus ut nisi venenatis, in feugiat urna condimentum. Morbi in velit rhoncus, convallis felis eu, dignissim nulla. Sed porta sit amet velit consequat congue. Quisque accumsan nibh ultricies turpis placerat, a maximus purus interdum. Mauris justo lorem, iaculis sit amet pharetra vel, accumsan eu sapien. Quisque viverra purus ac elit condimentum egestas.

        </div>
        <div class="col-3 border" style="background-color:LightYellow ;">

            <img class="card-img-fluid" src="{{ asset('img/nuotrauka4.png') }}" alt="Nuotrauka" style="max-width:100%">

        </div>
        <div class="col-2 border" style="background-color:Turquoise;">

            <button type="button" class="btn btn-primary border">Eiti į Įrašą</button>
            <button type="button" class="btn btn-primary border">Eiti į Komentarus</button>
            <button type="button" class="btn btn-primary border">Įrašo Data</button>
            <button type="button" class="btn btn-primary border">Įrašo Autorius</button>

        </div>

    </div>
    @endsection
